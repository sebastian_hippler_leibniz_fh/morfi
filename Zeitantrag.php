<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <Title>Zeitanträge</Title>
        <div class="navigation">
            <div class="image">
                <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
            <!-- SESSION initialisierung -->
                    <?php
                    $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                    if ($db->connect_error)
                    {
                        die("Verbindung fehlgeschlagen: " . $db->connect_error);
                    }
                    session_start();
                    if (isset($_POST["abmelden"]))
                    {
                        session_unset();
                        session_destroy();
                        header("Location: index.php");
                        die;
                    }
                    echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    $nutzer = array();
                    $benutzer=$_SESSION['benutzername'];
                    $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                    if($result = mysqli_query($db, $user))
                    {
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $nutzer[]=$row['Rolle'];
                        }
                    }

                    ?>

                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                    </tr>
                </table>
            <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">
        </div>
        </form>
    </head>
    
    <body>
        <script>
        </script>
    <div class="Zweitabellen">
        <div class="Linkeeingabe">
            <h2><u>Wiederkehrende Zeitanträge</u></h2>
            <table >
                <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
                    <tr>
                        <td class="tabelle1">Fehlgrund:</td><td class="tabelle1"><select name="fehlgrund">
                            <option value="Urlaub"> Urlaub </option>
                            <option value="Krank"> Krankheit </option>
                        </select></td></tr>
                        <tr><td class="tabelle1">Von:</td><td class="tabelle1"><input type="date" name="startd" required></td></tr>
                        <tr><td class="tabelle1">Bis:</td><td class="tabelle1"><input type="date" name="endd" required></td></tr>
                        <tr><td class="tabelle1">Halbtags?</td><td class="tabelle1"><input type="checkbox" name="zeitraum"></td></tr>

                        <tr><td class="tabelle1"><input type="submit" name="eintragen" class= "myButton"value="Eintragen"></td></tr>
                    </tr>
                </form>
            </table>
            <?php


                if (isset($_POST['eintragen']))
                {
                    $fehlgrund= $_POST['fehlgrund'];
                    $von = $_POST['startd'];
                    $bis = $_POST['endd'];
                    $nicht = "";

                    if(isset($_POST['zeitraum']))
                    {
                        $zeitraum = 4;
                    }
                    else
                    {
                        $zeitraum = 8;
                    }
                    if($von != $bis && $zeitraum == 4)
                    {
                        echo "Halbtagsangaben gehen nur bei Einzelanträgen!";
                        die;
                    }
                   /* $pro = "SELECT * FROM projekt WHERE ID = '32'";
                    $proarray=array();
                    if($result = mysqli_query($db, $pro))
                    {
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $proarray[]=$row;
                        }
                    }*/
                    $arb = "SELECT * FROM arbeitspakete WHERE Benutzername = '".$_SESSION["benutzername"]."' AND Projekt_ID = '32' AND (Bezeichnung = 'Urlaub' OR Bezeichnung ='Krankheit') ORDER BY Bezeichnung ASC";
                    $arbarray=array();
                    if($result = mysqli_query($db, $arb))
                    {
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $arbarray[]=$row;
                        }
                    }

                    if($fehlgrund == "Krank")
                    {
                        $datumwe = array();
                        $dat1 = strtotime($von);
                        $dat2 = strtotime($bis);
                        $datediff = $dat2 - $dat1;
                        $zwischen = round($datediff / (60 * 60 * 24))+1;
                        for($i = 0; $i < $zwischen; $i++)
                        {
                            $wochenende = date('w', strtotime($von." +$i day"));
                            if($wochenende == 0 || $wochenende == 6)
                            {
                                continue;
                            }
                            else
                            {
                                $datumwe[] = date("Y-m-d", strtotime("+$i day"));
                            }
                        }
                        $endstunden = count($datumwe) * $zeitraum;
                        $apid=$arbarray[0]['ID'];
                        $insert = "INSERT INTO arbeitszeit (Datum, DatumBis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Zeitstatus, Abwesenheit)
                         VALUES ('$von', '$bis', '32', '$apid', '$endstunden', '".$_SESSION["benutzername"]."','$nicht','1', '$fehlgrund')";

                        if ($db->query($insert) === TRUE)
                        {
                           echo "Fehlgrund wurde eingetragen!";
                        }
                        else
                        {
                            echo $db -> error;
                        }
                    }
                    if($fehlgrund == "Urlaub")
                    {
                        $datumwe = array();
                        $dat1 = strtotime($von);
                        $dat2 = strtotime($bis);
                        $datediff = $dat2 - $dat1;
                        $zwischen = round($datediff / (60 * 60 * 24))+1;
                        for($i = 0; $i < $zwischen; $i++)
                        {
                            $wochenende = date('w', strtotime($von." +$i day"));
                            if($wochenende == 0 || $wochenende == 6)
                            {
                                continue;
                            }
                            else
                            {
                                $datumwe[] = date("Y-m-d", strtotime("+$i day"));
                            }
                        }
                        $endstunden = count($datumwe) * $zeitraum;
                        $apid=$arbarray[1]['ID'];
                        $insert = "INSERT INTO arbeitszeit (Datum, DatumBis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Zeitstatus, Abwesenheit)
                         VALUES ('$von', '$bis', '32', '$apid', '$endstunden', '".$_SESSION["benutzername"]."','$nicht','1', '$fehlgrund')";

                        if ($db->query($insert) === TRUE)
                        {
                           //echo "Fehlgrund wurde eingetragen!";
                        }
                        else
                        {
                            echo $db -> error;
                        }
                        $jahr= substr($von, 0, 4);
                        $jahrbis= substr($bis, 0, 4);

                        if ( $jahr == $jahrbis)
                        {
                            $Jahr1 = $jahr -1;
                        }
                        else
                        {
                            $Jahr1 = $jahr;
                        }

                        $selectzeit= "SELECT * FROM Urlaub WHERE BenutzerID = '".$_SESSION["ID"]."' AND ( Urlaubsjahr = '".$Jahr1."' OR Urlaubsjahr = '".$jahrbis."' )";
                        $zeittabelle = array();
                        if($result = mysqli_query($db, $selectzeit))
                        {
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $zeittabelle[] = $row;
                            }
                        }
                        $selectrest= "SELECT Resturlaub FROM Urlaub WHERE BenutzerID = '".$_SESSION["ID"]."' AND Urlaubsjahr = $jahr-1";
                        $resttabelle = array();
                        if($result = mysqli_query($db, $selectrest))
                        {
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $resttabelle[] = $row;
                            }
                        }
                        if($resttabelle == NULL)
                        {
                            $rest = 0;
                        }
                        else
                        {
                            $rest = $resttabelle[0]["Resturlaub"];
                        }
                        $selectuser= "SELECT Urlaubstage FROM mitarbeiter WHERE ID = '".$_SESSION["ID"]."'";
                        $usertabelle = array();
                        if($result = mysqli_query($db, $selectuser))
                        {
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $usertabelle[] = $row;
                            }
                        }

                        if($zeittabelle == NULL)
                        {
                            $insertzeit = "INSERT INTO Urlaub (BenutzerID, Urlaubstage, Resturlaub, Urlaubsjahr)
                                            VALUES ('".$_SESSION["ID"]."', '".$usertabelle[0]["Urlaubstage"]."', '".$usertabelle[0]["Urlaubstage"]."', '$jahr')";

                            if ($db->query($insertzeit) === TRUE)
                            {
                                if($jahr!=$jahrbis)
                                {
                                    $insertbis = "INSERT INTO Urlaub (BenutzerID, Urlaubstage, Resturlaub, Urlaubsjahr)
                                    VALUES ('".$_SESSION["ID"]."', '".$usertabelle[0]["Urlaubstage"]."', '".$usertabelle[0]["Urlaubstage"]."', '$jahrbis')";

                                    if ($db->query($insertbis) === TRUE)
                                    {
                                        echo "Urlaub aktualisiert!";
                                    }
                                    else
                                    {
                                        echo $db -> error;
                                    }
                                }
                                else
                                {
                                    echo "Urlaub aktualisiert!";
                                }
                            }
                                else
                                {
                                    echo $db -> error;
                                }

                        }

                        $Datum = array();
                        $monat = array();
                        for($i = 0; $i < $zwischen; $i++)
                        {
                            $wochenende = date('w', strtotime($von."+$i day"));
                            if($wochenende == 0 || $wochenende == 6)
                            {
                                continue;
                            }
                            else
                            {

                                $Datum[date("Y", strtotime($von."+$i day"))][] = date("m", strtotime($von."+$i day"));
                            }
                        }
                        $anzahldaten = "";
                        $anzahlmonat = "";
                        $restzeit= date("01-04-Y", strtotime($bis));
                        $restzeit = date("m", strtotime($restzeit));
                        $hilfearray = array();
                        $hilfsvar = 0;
                        foreach($Datum as $index => $value)
                        {
                            $hilfe = 0;
                            foreach($value as $innerer_index => $innerer_value)
                            {
                                if($innerer_value<$restzeit)
                                {
                                    $wert = "VOR";
                                }
                                else
                                {
                                    $wert = "NACH";
                                }
                                //echo $index.",".$innerer_value."<br>";
                                $hilfe++;
                            }
                            $hilfearray[$index] = $hilfe.",".$wert;
                        }

                        $finalarray = array();
                        foreach($hilfearray as $index => $value)
                        {
                            $finalarray[$index] = explode(",", $hilfearray[$index]);
                        }

                        foreach($finalarray as $index => $value)
                        {
                            $selectzeit= "SELECT * FROM Urlaub WHERE BenutzerID = '".$_SESSION["ID"]."'";
                            $zeittabelle = array();
                            if($result = mysqli_query($db, $selectzeit))
                            {
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    $zeittabelle[] = $row;
                                }
                            }
                            if ( $zeitraum == "4" )
                            {
                                $value[0]= $value[0]/2;
                            }

                            if ( $value[1]=="VOR")
                            {

                                if( $zeittabelle[0]["Resturlaub"] - $value[0] >= 0)
                                {
                                    $updatezeit = "UPDATE `Urlaub` SET Resturlaub = Resturlaub - $value[0] WHERE BenutzerID = '".$_SESSION["ID"]."' AND Urlaubsjahr = ($index-1)";
                                    if ($db->query($updatezeit) === TRUE)
                                    {
                                        if($hilfsvar == 0)
                                        {
                                            echo "Ihr Urlaub wurde aktualisiert 1";
                                            //$hilfsvar = $hilfsvar + 1;
                                        }
                                    }
                                    else
                                    {
                                        echo $db -> error;
                                    }
                                }
                                if( $zeittabelle[0]["Resturlaub"] - $value[0] < 0)
                                {
                                    $differenz = $zeittabelle[0]["Resturlaub"] - $value[0];
                                    $updatezeit = "UPDATE `Urlaub` SET Resturlaub = 0 WHERE BenutzerID = '".$_SESSION["ID"]."' AND Urlaubsjahr = $index-1";
                                    if ($db->query($updatezeit) === TRUE)
                                    {
                                        if($hilfsvar == 0)
                                        {
                                            echo "Ihr Urlaub wurde aktualisiert 2";
                                            //$hilfsvar = $hilfsvar + 1;
                                        }
                                    }
                                    else
                                    {
                                        echo $db -> error;
                                    }
                                    if($zeittabelle[1]["Resturlaub"] + $differenz <= 0)
                                    {
                                        echo "Sie haben zuviele Urlaubstage eingetragen!";
                                        break;
                                    }
                                    $updatezeit = "UPDATE `Urlaub` SET Resturlaub = Resturlaub + $differenz WHERE BenutzerID = '".$_SESSION["ID"]."' AND Urlaubsjahr = $index ";
                                    if ($db->query($updatezeit) === TRUE)
                                    {
                                        if($hilfsvar == 0)
                                        {
                                            echo "Ihr Urlaub wurde aktualisiert 3";
                                            //$hilfsvar = $hilfsvar + 1;
                                        }
                                    }
                                    else
                                    {
                                        echo $db -> error;
                                    }
                                }
                            }
                            else
                            {
                                if($zeittabelle[1]["Resturlaub"] - $value[0] <= 0 )
                                {
                                    echo "Sie haben zuviele Urlaubstage eingetragen!";
                                    break;
                                }
                                else
                                {
                                    $updatezeit = "UPDATE `Urlaub` SET Resturlaub = Resturlaub - $value[0] WHERE BenutzerID = '".$_SESSION["ID"]."' AND Urlaubsjahr = $index";
                                        if ($db->query($updatezeit) === TRUE)
                                        {
                                            echo "Ihr Urlaub wurde aktualisiert";
                                        }
                                        else
                                        {
                                            echo $db -> error;
                                        }
                                }
                            }

                        }
                }
                }
                $sql = "SELECT * FROM Zeituebersicht WHERE Benutzername = '".$_SESSION["benutzername"]."'";
                $abwesendheitstabelle = array();
                if ($result = mysqli_query($db, $sql))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $abwesendheitstabelle[]=$row;
                    }

                }
                else
                {
                    echo mysqli_error($db);
                }


            ?>
        </div>
                    
        <div class="Rechteeingabe">
            <h2><u>Wiederkehrende Ereignisse</u></h2>
                <table>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                <tr><td class="tabelle1">Fehlgrund:</td><td class="tabelle1">
                    <select name="grund">
                        <option value="Schule">Schule</option>
                        <option value="Universität">Universität</option>
                        <option value="Homeoffice">Homeoffice</option>
                        <option value="Freistellung">Freigestellt</option>
                    </select>
                </td></tr>
                <tr><td class="tabelle1">Von:</td><td class="tabelle1"><input type="date" name="datvon"></td></tr>
                <tr><td class="tabelle1">Bis:</td><td class="tabelle1"><input type="date" name="datbis"></td></tr>
                <tr><td class="tabelle1">Wiederkehrend:</td><td class="tabelle1">
                    <select name="wieder">
                        <option value="7">Wöchentlich</option>
                        <option value="14">Alle zwei Wochen</option>
                    </select>
                </td></tr>
                <tr><td class="tabelle1">Montag: </td><td class="tabelle1"><input type="checkbox" name="montag" value="1"></td></tr>
                <tr><td class="tabelle1">Dienstag: </td><td class="tabelle1"><input type="checkbox" name="dienstag" value="2"></td></tr>
                <tr><td class="tabelle1">Mittwoch: </td><td class="tabelle1"><input type="checkbox" name="mittwoch" value="3"></td></tr>
                <tr><td class="tabelle1">Donnerstag: </td><td class="tabelle1"><input type="checkbox" name="donnerstag" value="4"></td></tr>
                <tr><td class="tabelle1">Freitag: </td><td class="tabelle1"><input type="checkbox" name="freitag" value="5"></td></tr>
                <tr><td class="tabelle1"><input type="submit" name="eintrag" class= "myButton" value="Eintragen"></td></tr>
                </form>
                </table>
                <?php
                $wopro=  "SELECT * FROM arbeitspakete WHERE Benutzername = '".$_SESSION["benutzername"]."' AND (Bezeichnung = 'Schule' OR Bezeichnung = 'Universität' OR Bezeichnung = 'Homeoffice nicht zugeordnet' OR Bezeichnung = 'Freistellung')";
                $woproarray=array();
                if($result = mysqli_query($db, $wopro))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $woproarray[$row['ID']][$row['Bezeichnung']]=$row['Projekt_ID'];
                    }
                }
                if(isset($_POST['eintrag']))
                {
                    $grund = $_POST['grund'];
                    $datvon = $_POST['datvon'];
                    $datbis = $_POST['datbis'];
                    $wieder = $_POST['wieder'];
                    $monhilfe = 0;
                    $dihilfe = 0;
                    $mihilfe = 0;
                    $dohilfe = 0;
                    $frihilfe = 0;
                    if(isset($_POST['montag']))
                    {
                        $mon = $_POST['montag'];
                    }
                    else
                    {
                        $mon = "";
                    }
                    if(isset($_POST['dienstag']))
                    {
                        $di = $_POST['dienstag'];
                    }
                    else
                    {
                        $di = "";
                    }
                    if(isset($_POST['mittwoch']))
                    {
                        $mi = $_POST['mittwoch'];
                    }
                    else
                    {
                        $mi = "";
                    }
                    if(isset($_POST['donnerstag']))
                    {
                        $do = $_POST['donnerstag'];
                    }
                    else
                    {
                        $do = "";
                    }
                    if(isset($_POST['freitag']))
                    {
                        $fri = $_POST['freitag'];
                    }
                    else
                    {
                        $fri = "";
                    }

                    $dat1 = strtotime($datvon);
                    $dat2 = strtotime($datbis);
                    $datediff = $dat2 - $dat1;
                    $zwischen = round($datediff / (60 * 60 * 24))+1;
                    $zeitstunden= $zwischen * 8;
                    $nichts="";
                    $eins =1;
                    for($i = 0; $i<$zwischen; $i++)
                    {
                        $date = date("Y-m-d",strtotime($datvon."+$i day"));
                        if($mon == date("w",strtotime($datvon."+$i day")))
                        {

                            if($wieder == 14 && $monhilfe == 0)
                            {

                                $monhilfe = 1;
                                continue;
                            }
                            else
                            {
                                foreach($woproarray as $arbid => $arbbezeichnung)
                                {
                                    foreach($arbbezeichnung as $index => $projekt_id)
                                    {
                                        echo $index." ".$grund."<br>";
                                        if($index == $grund)
                                        {
                                            $insertzeit= $db->prepare("INSERT INTO arbeitszeit (Datum, Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Abwesenheit, Zeitstatus)
                                                            VALUES (?,?,?,?,?,?,?,?,?)");
                                            $insertzeit->bind_param("ssiiisssi", $date, $date, $projekt_id, $arbid, $zeitstunden, $_SESSION['benutzername'],$nichts,$grund,$eins);
                                            if($insertzeit->execute())
                                            {
                                                echo "";
                                                $monhilfe = 0;
                                            }
                                            else
                                            {
                                                echo $db->error;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if($di == date("w",strtotime($datvon."+$i day")))
                        {

                            if($wieder == 14 && $dihilfe == 0)
                            {

                                $dihilfe = 1;
                                continue;
                            }
                            else
                            {
                                foreach($woproarray as $arbid => $arbbezeichnung)
                                {
                                    foreach($arbbezeichnung as $index => $projekt_id)
                                    {
                                        echo $index." ".$grund."<br>";
                                        if($index == $grund)
                                        {
                                            $insertzeit= $db->prepare("INSERT INTO arbeitszeit (Datum, Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Abwesenheit, Zeitstatus)
                                                            VALUES (?,?,?,?,?,?,?,?,?)");
                                            $insertzeit->bind_param("ssiiisssi", $date, $date, $projekt_id, $arbid, $zeitstunden, $_SESSION['benutzername'],$nichts,$grund,$eins);
                                            if($insertzeit->execute())
                                            {
                                                echo "";
                                                $dihilfe = 0;
                                            }
                                            else
                                            {
                                                echo $db->error;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if($mi == date("w",strtotime($datvon."+$i day")))
                        {

                            if($wieder == 14 && $mihilfe == 0)
                            {

                                $mihilfe = 1;
                                continue;
                            }
                            else
                            {
                                foreach($woproarray as $arbid => $arbbezeichnung)
                                {
                                    foreach($arbbezeichnung as $index => $projekt_id)
                                    {
                                        if($index == $grund)
                                        {
                                            $insertzeit= $db->prepare("INSERT INTO arbeitszeit (Datum, Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Abwesenheit, Zeitstatus)
                                                            VALUES (?,?,?,?,?,?,?,?,?)");
                                            $insertzeit->bind_param("ssiiisssi", $date, $date, $projekt_id, $arbid, $zeitstunden, $_SESSION['benutzername'],$nichts,$grund,$eins);
                                            if($insertzeit->execute())
                                            {
                                                echo "";
                                                $mihilfe = 0;
                                            }
                                            else
                                            {
                                                echo $db->error;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if($do == date("w",strtotime($datvon."+$i day")))
                        {

                            if($wieder == 14 && $dohilfe == 0)
                            {

                                $dohilfe = 1;
                                continue;
                            }
                            else
                            {
                                foreach($woproarray as $arbid => $arbbezeichnung)
                                {
                                    foreach($arbbezeichnung as $index => $projekt_id)
                                    {
                                        echo $index." ".$grund."<br>";
                                        if($index == $grund)
                                        {
                                            $insertzeit= $db->prepare("INSERT INTO arbeitszeit (Datum, Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Abwesenheit, Zeitstatus)
                                                            VALUES (?,?,?,?,?,?,?,?,?)");
                                            $insertzeit->bind_param("ssiiisssi", $date, $date, $projekt_id, $arbid, $zeitstunden, $_SESSION['benutzername'],$nichts,$grund,$eins);
                                            if($insertzeit->execute())
                                            {
                                                echo "";
                                                $dohilfe = 0;
                                            }
                                            else
                                            {
                                                echo $db->error;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if($fri == date("w",strtotime($datvon."+$i day")))
                        {

                            if($wieder == 14 && $frihilfe == 0)
                            {

                                $frihilfe = 1;
                                continue;
                            }
                            else
                            {
                                foreach($woproarray as $arbid => $arbbezeichnung)
                                {
                                    foreach($arbbezeichnung as $index => $projekt_id)
                                    {
                                        echo $index." ".$grund."<br>";
                                        if($index == $grund)
                                        {
                                            $insertzeit= $db->prepare("INSERT INTO arbeitszeit (Datum, Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Abwesenheit, Zeitstatus)
                                                            VALUES (?,?,?,?,?,?,?,?,?)");
                                            $insertzeit->bind_param("ssiiisssi", $date, $date, $projekt_id, $arbid, $zeitstunden, $_SESSION['benutzername'],$nichts,$grund,$eins);
                                            if($insertzeit->execute())
                                            {
                                                echo "";
                                                $frihilfe = 0;
                                            }
                                            else
                                            {
                                                echo $db->error;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                ?>
        </div>
    </div>
            <h2>Geplante Zeiten</h2>
            <div class="wrapper">
        <table class="scroll" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th>Datum(von)</th>
                            <th>Datum(bis)</th>
                          <!-- <th class="tabelle">Planung</th>-->
                            <th>Projekt</th>
                            <th>Arbeitspaket</th>
                            <th>Grund</th>
                            <th>Beschreibung</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                        foreach($abwesendheitstabelle as $value)
                        {
                            echo '<form action="Zeitantragbearbeiten.php" method="POST" id="area">';
                            if($value["Zeitstatus"] == 1)
                            {
                                $planung = "geplant";
                            }
                            else
                            {
                                //$planung = "nicht geplant";
                                continue;
                            }
                            $_SESSION['AP_ID']=$value['AP-ID'];
                            $_SESSION['Projekt_ID']=$value['Projekt-ID'];
                            echo '<tr><input type="hidden" name="planung" value="'.$value["Zeitstatus"].'"><input type="hidden" name="id" value="'.$value["ID"].'"><input type="hidden" name="stunden" value="'.$value["Stunden"].'">';
                            echo '<td><input type="hidden" name="datumvon" value="'.$value["Datum"].'">'.$value["Datum"].'</td>';
                            echo '<td><input type="hidden" name="datumbis" value="'.$value["Datumbis"].'">'.$value["Datumbis"].'</td>';
                            //echo '<td class="tabelle1">'.$planung.'</td>';
                            echo '<td><input type="hidden" name="projektname" value="'.$value["Projektname"].'">'.$value["Projektname"].'</td>';
                            echo '<td><input type="hidden" name="ap" value="'.$value["Arbeitspaket"].'">'.$value["Arbeitspaket"].'</td>';
                            echo '<td><input type="hidden" name="abwesenheit" value="'.$value["Abwesenheit"].'">'.$value["Abwesenheit"].'</td>';
                            echo '<td><input type="hidden" name="beschreibung" value="'.$value["Beschreibung"].'">'.$value["Beschreibung"].'</td>';
                            echo '<td><input type="submit" class="myButton" name="bearbeiten" value="Bearbeiten"></td>';?>
                                    <td><input type="submit" class="abrechnung" name="inplanung" value="In Ist-Zeit umwandeln" onclick='return confirm("Sind Sie sicher, dass Sie diesen Antrag umwandeln wollen?")'></td>
                                    <td><input type="submit" class="entfernen" name="verwerfen" value="Verwerfen" onclick='return confirm("Sind Sie sicher, dass Sie diesen Antrag entfernen wollen?")'></td></tr></form>
                                    <?php
                        }

                    ?>
                    </tbody>
                </table>
            </div>
    </body>
</html>
