<!DOCTYPE html>
<html>
    <head>
        <title> Arbeitszeiterfassung </title>
        <div class="navigation">
            <div class="image">
                        <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
                    <?php

                // DB Verbindung 
                    $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");;
                    if ($db->connect_error)
                    {
                        die("Verbindung fehlgeschlagen: " . $db->connect_error);
                    }
                // SESSION initialisierung
                    session_start();
                    if(isset($_SESSION["Projekt_Temp"]))
                    {
                        $insert=$db->prepare("DELETE FROM Projekt_Temp Where Projekt_ID = ?");
                        $insert->bind_param("s", $_SESSION["Projekt_Temp"]);
                        if($insert->execute())
                        {
                            
                            unset($_SESSION["Projekt_Temp"]);
                        }

                    }
                        // Logoutfunktion
                        if (isset($_POST["abmelden"]))
                        {
                            session_unset();
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }

                        // Kontrollabfrage, Zugriff nur durch ein Login-möglich
                        if (!isset($_SESSION["benutzername"]))
                        {
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }
                        echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                        $ID=$_SESSION["ID"];
                        //$_SESSION["benutzername"] = "Paetsch";
                        //$_SESSION["ID"] = 5;
                        $nutzer = array();
                        $benutzer=$_SESSION['benutzername'];
                        $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                        if($result = mysqli_query($db, $user))
                        {
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $nutzer[]=$row['Rolle'];
                            }
                        }
                    ?>
                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                       
                    </tr>
                </table>
                <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">      
                </form>
        </div>
        <div>
            <h2><u>Arbeitszeiterfassung IFANO</u></h2>
        </div>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <!--AJAX Life-Daten aus der Datenbank Funktion -->
            <script>
            function showfertigstellung(str) 
            {
                if (str == "") 
                {
                    document.getElementById("fertigstellen").innerHTML = "";
                    return;
                } 
                else 
                {
                    if (window.XMLHttpRequest) 
                    {
                        xmlhttp = new XMLHttpRequest();
                    } 
                    else 
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() 
                    {
                        if (this.readyState == 4 && this.status == 200) 
                        {
                            document.getElementById("fertigstellen").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET","fertigstellungajax.php?q="+str, true);
                    xmlhttp.send();
                }
            }
            function showpaket(str) 
            {
                if (str == "") 
                {
                    document.getElementById("paket").innerHTML = "";
                    return;
                } 
                else 
                {
                    if (window.XMLHttpRequest) 
                    {
                        xmlhttp = new XMLHttpRequest();
                    } 
                    else 
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() 
                    {
                        if (this.readyState == 4 && this.status == 200) 
                        {
                            document.getElementById("paket").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET","arbeitspaketajax.php?q="+str, true);
                    xmlhttp.send();
                }
            }
            function updateTextInput(val) 
            {
                document.getElementById('textInput').value=val; 
            }
            function updatedate(test)
            {
                var tage = document.getElementById('1').value=test;
                document.getElementById('2').value = tage;
            }
            </script>
        <?php


            //Verbindung zur Datenbank
                $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                if ($db->connect_error)
                {
                    die("Verbindung fehlgeschlagen: " . $db->connect_error);
                }
                if (isset($_POST['einfuegen']))
                {
                    //If-Abfrage zur Fehlervermeidung bei Checkboxen
                    if(!isset($_POST['sonstiges']))
                    {
                        $sonstigebesch = "";
                    }
                    else
                    {
                        $sonstigebesch = $_POST['beschreibung'];
                    }
                    if(!isset($_POST['code']))
                    {
                        $code = "";
                    }
                    else
                    {
                        $code = $_POST['code']."\n";
                    }
                    if(!isset($_POST['schulung']))
                    {
                        $schulung = "";
                    }
                    else
                    {
                        $schulung = $_POST['schulung']."\n";
                    }
                    if(!isset($_POST['dokumentation']))
                    {
                        $doku = "";
                    }
                    else
                    {
                        $doku = $_POST['dokumentation']."\n";
                    }
                    // Zeitstatus
                    if(!isset($_POST['Zeitstatus']))
                    {
                        $Zeitstatus = "0"; // nicht geplant
                    }
                    else
                    {
                        $Zeitstatus = "1"; // geplant
                    }



                    // Eingabe Nutzer
                    $datum= $_POST['datum'];
                    $datumbis = $_POST['datumbis'];
                    $zeita= $_POST['zeitbeginn'];
                    $projekt= $_POST['projekt'];
                    $arbeitspaket= $_POST['arbeitspaket'];
                    $beschreibung= $doku.$schulung.$code."Sonstiges: ".$sonstigebesch;
                    $fertigstellung= $_POST['fertigstellung'];
                    $Abwesendheit = $_POST['Abwesend'];

                    $datumarr=array();
                    $datediff = strtotime($datumbis) - strtotime($datum);
                    $zwischen = round($datediff / (60 * 60 * 24))+1;
                    for($i = 0; $i < $zwischen; $i++)
                    {
                        $wochenende = date('w', strtotime($datum."+$i day"));
                        if($wochenende == 0 || $wochenende == 6)
                        {
                            continue;
                        }
                        else
                        {
                            
                            $datumarr[] = date("m", strtotime($datum."+$i day"));
                        }
                    }
                    $zeitzwischen = count($datumarr);
                    


                    //Rechnung Zeit

                    $stunden = substr($zeita, 0, 2);
                    $stunden = $stunden + substr($zeita,3,2)/60;
                    $stunden = $stunden * $zeitzwischen;

                    // Anmeldung Datenbank mit "Einfügen-Funktion"
                    $sql = mysqli_query($db, "INSERT INTO arbeitszeit(Datum,Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Zeitstatus, Abwesenheit)
                    VALUES ('$datum','$datumbis','$projekt','$arbeitspaket','$stunden','".$_SESSION["benutzername"]."','$beschreibung','$Zeitstatus','$Abwesendheit')");

                    $update = mysqli_query($db, "UPDATE arbeitspakete SET Stadium = '$fertigstellung' WHERE ID = '$arbeitspaket'");

                }
                $abwesend= "SELECT * FROM Abwesenheitsstatus WHERE (Abwesenheitsstatus != 'Urlaub' AND Abwesenheitsstatus != 'Krank') ";
                $abwesenheitsstatus = array();

                if ($result = mysqli_query($db, $abwesend))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $abwesenheitsstatus[]=$row;
                    }
                    
                }
            //Nimmt alle Datenbankeinträge die das Stadium Realiserung und Wo der Benutzer ein Arbeitspaket oder Projekt zugeordnet ist               
                $user= "SELECT * from  Arbeiterzuordnung WHERE projektleiter ='".$_SESSION["benutzername"]."'";
                $allenutzer=array();
                $anzahl = 0;



                if($result = mysqli_query($db, $user))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $allenutzer[]= "$row->ID $row->Projektname";
                    }
                }
                if($result = mysqli_query($db, $user))
                {
                    $anzahl = $result->num_rows;
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $ubersicht[$row['ID']]=$row;
                    }
                }
        ?>
        <form action ="<?php $_SERVER ['PHP_SELF'];?>" method="POST" id="area">
            <div>
                <table> <border="1" cellspacing="2" cellpadding="3">
                    <!--Eingabe Nutzer-->
                    <tr><td class="tabelleohneinput">Datum:</td><td class="tabelleohneinput"><input type = "Date" style="width:507px" name = "datum" id="1" onchange="updatedate(this.value)"></td></tr>
                    <tr><td class="tabelleohneinput">Bis:(optional)</td><td class="tabelleohneinput"><input type = "Date" style="width:507px" name = "datumbis" id="2"></td></tr>
                    <tr><td class="tabelleohneinput">Zeit:</td><td class="tabelleohneinput"><input type = "time" step="900" min="00:15" max="10:00" style="width:507px" name = "zeitbeginn"></td></tr>
                    <form>
                        <tr><td class="tabelleohneinput">Projekt:</td><td class="tabelleohneinput"><select name="projekt" style="width:515px" id="dropdown"onchange="showpaket(this.value) ">
                            <option value ="0" selected="selected"> Bitte wählen </option>
                            <?php
                                print_r($allenutzer);
                                foreach($allenutzer as $value)
                                {
                                    echo '<option value="'.$value.'">'.$value.'</option>';
                                }
                            ?>
                        </select></td>
                    </form>
                    <tr><td class="tabelleohneinput">Arbeitspaket:</td><td class="tabelleohneinput"><div id="paket"></div></td></tr>
                    <tr><td class="tabelleohneinput">Fertigstellung:</td><td class="tabelleohneinput">
                    <div id="fertigstellen"></div></td></tr>
                    <tr><td class="tabelleohneinput">Details:</td><td class="tabelleohneinput">
                        <select name="Abwesend">
                                <?php
                                    foreach($abwesenheitsstatus as $value)
                                    {
                                        if($value['Abwesenheitsstatus'] == "IFANO")
                                        {
                                            echo '<option value="'.$value['Abwesenheitsstatus'].'" selected>'.$value['Abwesenheitsstatus'].'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$value['Abwesenheitsstatus'].'">'.$value['Abwesenheitsstatus'].'</option>';
                                        }
                                    }
                                ?>
                        <input type="Checkbox" name="Zeitstatus" value="0">Geplante Arbeitszeit
                    <tr><td class="tabelleohneinput">Beschreibung:</td><td class="tabelleohneinput">
                        <input type="Checkbox" name="dokumentation" value="Dokumentation">Dokumentation
                        <input type="Checkbox" name="schulung" value="Schulung">Schulung
                        <input type="Checkbox" name="code" value="Code-Anpassungen">Code-Anpassungen
                        <input type="Checkbox" name="sonstiges" value="Sonstiges">Sonstiges<br>
                        <Textarea form="area" cols="10" rows="10" class="textbereichzeit" name = "beschreibung"></textarea></td></tr>
                    <tr><td class="tabelle" colspan="2"><input type= "submit" class="myButton" name = "einfuegen" value = "Einfügen"></td></tr>
                </table>
                <br>
            </div>
        </form>
        <div class="wrapper">
        <table class="scroll" cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th></th>
                    <th>Auftragsnummer</th>
                    <th>Projektname</th>
                    <th>Kunde</th>
                    <th>Geplantes Startdatum</th>
                    <th>Ist-Startdatum</th>
                    <th>Geplantes Enddatum</th>
                    <th>Ist-Enddatum</th>
                    <th>Stadium</th>
                    <th>Geplante Arbeitszeit (in h)</th>
                    <th>Ist-Arbeitszeit (in h)</th>
                    <th>Projektleiter</th>
                    <th>Projekt-beschreibung</th>
                    <th>Verweis-Managementsystem</th>
                    


                </tr>
                <tbody>
                    <?php
                        // erstell die Übersicht unter den Eingabefeldern
                        foreach($ubersicht as $value)
                        {
                            //$Zeit = floor($value['ist_arbeitszeit']) +( "0.".(fmod($value['ist_arbeitszeit'],1)*60));
                            echo '<form action="projektdetailansicht.php" method="POST">';
                            echo '<tr>
                                    <td><input type="submit" class="myButton" name="bearbeiten" value="Detailansicht"></td>
                                    <input type="hidden" name="ID" value="'.$value['ID'].'">
                                    <td><input type="hidden" name="Auftragsnummer" value="'.$value['Auftragsnummer'].'">'.$value['Auftragsnummer'].'</td>
                                    <td><input type="hidden" name="projektname" value="'.$value['Projektname'].'">'.$value['Projektname'].'</td>
                                    <td><input type="hidden" name="kunde" value="'.$value["Kunden_ID"].' '.$value['Firmenname'].'">'.$value['Firmenname'].'</td>
                                    <td><input type="hidden" name="gepstart" value="'.$value['gepl_Startdatum'].'">'.$value['gepl_Startdatum'].'</td>
                                    <td><input type="hidden" name="iststart" value="'.$value['ist_Startdatum'].'">'.$value['ist_Startdatum'].'</td>
                                    <td><input type="hidden" name="gepend" value="'.$value['gepl_Enddatum'].'">'.$value['gepl_Enddatum'].'</td>
                                    <td><input type="hidden" name="istend" value="'.$value['ist_Enddatum'].'">'.$value['ist_Enddatum'].'</td>
                                    <td><input type="hidden" name="stadium" value="'.$value['Stadium'].'">'.$value['Stadium'].'</td>
                                    <td align="right"><input type="hidden" name="geparbeit" value="'.$value['gepl_arbeitszeit'].'">'.number_format(floor($value['gepl_arbeitszeit']) +((fmod($value['gepl_arbeitszeit'],1)*0.6)),2,':','').'</td>
                                    <td align="right"><input type="hidden" name="istarbeit" value="'.$value['ist_arbeitszeit'].'">'.number_format(floor($value['ist_arbeitszeit']) +((fmod($value['ist_arbeitszeit'],1)*0.6)),2,':','').'</td>
                                    <td><input type="hidden" name="projektleiter" value="'.$value['Projektleiter'].'">'.$value['Projektleiter'].'</td>
                                    <td><input type="hidden" name="projektbesch" value="'.$value['Projektbeschreibung'].'">'.$value['Projektbeschreibung'].'</td>
                                    <td><input type="hidden" name="jira" value="'.$value['VerweisManagmentsystem'].'">'.$value['VerweisManagmentsystem'].'</td>
                                    <input type="hidden" name="kostenstelle" value="'.$value['Kostenstelle'].'">
                                    <input type="hidden" name="kostentraeger" value="'.$value['Kostentraeger'].'">
                                </tr>';
                            echo '</form>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
