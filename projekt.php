<!DOCTYPE HTML>
<html>
    <head>
        <div class="navigation">
            <div class="image">
                    <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
            <?php

                // DB Verbindung 
                    $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");;
                    if ($db->connect_error)
                    {
                        die("Verbindung fehlgeschlagen: " . $db->connect_error);
                    }
                // SESSION initialisierung
                session_start();
                if(isset($_SESSION["Projekt_Temp"]))
                {
                    $insert=$db->prepare("DELETE FROM Projekt_Temp Where Projekt_ID = ?");
                    $insert->bind_param("s", $_SESSION["Projekt_Temp"]);
                    if($insert->execute())
                    {
                        
                        unset($_SESSION["Projekt_Temp"]);
                    }

                }

                echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    // Logout Button
                    if (isset($_POST['abmelden']))
                    {
                        session_unset();
                        session_destroy();
                        header("Location: index.php");
                        die;
                    }
                    // Session Abfrage
                        if (!isset($_SESSION["benutzername"]))
                        {
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }
                        $nutzer = array();
                        $benutzer=$_SESSION['benutzername'];
                        $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                        if($result = mysqli_query($db, $user))
                        {
                            while($row = mysqli_fetch_assoc($result))
                            {
                                $nutzer[]=$row['Rolle'];
                            }
                        }

                        // Anlegefunktion
                        if (isset($_POST['anlegen']))
                        {
                            $bezeichnung=$_POST['bezeichnung'];
                            $gepstart=$_POST['start'];
                            $gepend=$_POST['end'];
                            $arbeitszeit=$_POST['arbeitszeit'];
                            $beschreibung=$_POST['beschreibung'];
                            $projektleiter=$_POST['leiter'];
                            $projektkunde=$_POST['kunde'];
                            $stadium=$_POST['stadium'];
                            $auftragsnummer = $_POST['auftragsnummer'];
                            $Kostenstelle =  $_SESSION['kostenstelle'];
                            $Kostentraeger =  $_SESSION['kostentraeger'];
                            $jira = $_POST['jira'];
                            $Aktiv = 1;
                            $_SESSION["Projektname"] = $bezeichnung;
                            $_SESSION["Auftragsnummer"] = $auftragsnummer;
                            $_SESSION["gepl_startdatum"] = $gepstart;
                            $_SESSION["gepl_enddatum"] = $gepend;
                            $_SESSION["gepl_arbeitszeit"] = $arbeitszeit;
                            $_SESSION["Projektleiter"] = $projektleiter;
                            $_SESSION["stadium"] = $stadium;
                            $_SESSION["jira"] = $jira;
                            $_SESSION["Kostenträger"]=$Kostentraeger;
                            $_SESSION["Kostenstelle"]=$Kostenstelle;
                            $_SESSION["Projektbeschreibung"]=$beschreibung;
                            $_SESSION["Kunden_ID"] = $projektkunde;


                            $insert=$db->prepare("INSERT INTO projekt (Auftragsnummer, Projektname, Kunden_ID, gepl_Startdatum,
                                                gepl_Enddatum, gepl_arbeitszeit, Projektleiter, Projektbeschreibung, Stadium, Kostenstelle, Kostentraeger, Aktiv, VerweisManagmentsystem)
                                                VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
                            $insert->bind_param("sssssssssssss", $auftragsnummer, $bezeichnung, $projektkunde,
                                                $gepstart, $gepend, $arbeitszeit, $projektleiter,
                                                $beschreibung, $stadium, $Kostenstelle, $Kostentraeger, $Aktiv, $jira);
                            if($insert->execute())
                            {
                                $select=$db->prepare("SELECT * FROM projekt WHERE Projektname = ?");
                                $select->bind_param("s", $bezeichnung);
                                $select->execute();
                                $result = $select->get_result();
                                while($row = $result->fetch_assoc())
                                {
                                    $_SESSION["ProjektID"] = $row['ID'];
                                }
                                header("Location: projektdetailansicht.php");
                                
                            }
                            else
                            {
                                echo "Projektanlage fehlgeschlagen!";
                                echo $db -> error;
                            }
                            
                        }
            ?>
            <!-- Headerbuttons -->
            <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                       
                    </tr>
                </table>
                <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">
                </form>
                <!-- Popup Löschfenster -->
                    <script>   
                        function popup()
                        {
                            var txt;
                            var r = confirm("Soll das Projekt wirklich gelöscht werden?(Alle damit verbundenen Arbeitspakete gehen hiermit verloren.");
                            if (r == true) {
                                txt = true;
                                return true;
                            } else {
                                txt = false;
                                return false;
                            }
                            document.getElementById("demo").innerHTML = txt;
                        }
                        function showkunde(str) 
                        {
                            if (str == "") 
                            {
                                document.getElementById("kundedrop").innerHTML = "";
                                return;
                            } 
                            else 
                            {
                                if (window.XMLHttpRequest) 
                                {
                                    xmlhttp = new XMLHttpRequest();
                                } 
                                else 
                                {
                                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                }
                                xmlhttp.onreadystatechange = function() 
                                {
                                    if (this.readyState == 4 && this.status == 200) 
                                    {
                                        document.getElementById("kundedrop").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET","kundeajax.php?q="+str, true);
                                xmlhttp.send();
                            }
                        }
                    </script>
        </div>
        <?php


            //Abfrage aller Mitarbeiter
                $query=  "SELECT * FROM mitarbeiter";
                $leiter=array();

                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $leiter[]="$row->Benutzername";
                    }
                }
            //Abfrage aller Kunden
                $kunde=  "SELECT * FROM kunde WHERE Aktiv = 1";
                $kundeprojekt=array();

                if($result = mysqli_query($db, $kunde))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $kundeprojekt[]="$row->ID $row->Firmenname $row->Zusatz";
                    }
                }

            // Abfrage Kostenstelle
                $query= "SELECT * FROM Kostenstellen";
                $kostenstelle = array();

                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $kostenstelle[]="$row->Kostenstelle";
                    }
                }
            // Abfrage Kostenstelle
                $query= "SELECT * FROM Kostentraeger";
                $kostentraeger = array();

                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $kostentraeger[]="$row->Kostentraeger";
                    }
                }

            // Löschen/ Inaktiv setzen 
            
                if(isset($_POST['loeschen']))
                {
                    if($_POST['loeschen']==true)
                    {
                            $projektid = $_POST['ID'];
                            $projektinaktiv = "UPDATE projekt Set Aktiv = 0 WHERE ID = ?";
                            $APInaktiv = "UPDATE arbeitspakete Set Aktiv = 0 WHERE Projekt_ID = ?";
                            $delete= $db->prepare($projektinaktiv);
                            $delete-> bind_param("i", $projektid);
                            if($delete->execute())
                            {
                                $deleteap= $db->prepare($APInaktiv);
                                $deleteap->bind_param("i", $projektid);
                                if($deleteap->execute())
                                {
                                    echo "Das Projekt und alle zugehörigen Arbeitspakete wurden entfernt.";
                                }
                            }
                            else
                            {
                                echo "Beim Entfernen ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe";
                            }
                        }
                }

                

                if(isset($_POST['abschliessen']))
                {
                    if($_POST['abschliessen']==true)
                    {
                            $projektid = $_POST['ID'];
                            $projektinaktiv = "UPDATE projekt Set Stadium = 'Abgeschlossen' WHERE ID = ?";
                            $APInaktiv = "UPDATE arbeitspakete Set Stadium = 100 WHERE Projekt_ID = ?";
                            $delete= $db->prepare($projektinaktiv);
                            $delete-> bind_param("i", $projektid);
                            if($delete->execute())
                            {
                                $deleteap= $db->prepare($APInaktiv);
                                $deleteap->bind_param("i", $projektid);
                                if($deleteap->execute())
                                {
                                    echo "Das Projekt und alle zugehörigen Arbeitspakete wurden entfernt.";
                                }
                            }
                            else
                            {
                                echo "Beim Entfernen ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe";
                            }
                        }
                }


        ?>
            <meta charset="utf-8">
            <link rel="stylesheet" href="style.css">
            <Title>Projekterfassung</Title>
    </head>
    <h2><u>Projekterfassung</u></h2>
    <body>
        <!-- Eingabetabelle -->
            <div>
                <table> <border="1" cellspacing="2" cellpadding="3">
                    <form action="projekt.php" method="POST" id="area">
                        <tr><td class="tabelleohneinput"><b>Auftragsnummer:</b></td><td class="tabelleohneinput"><input type="text" style="width:201px" name="auftragsnummer" required></td></tr>
                        <tr><td class="tabelleohneinput"><b>Bezeichnung des Projektes:</b></td><td class="tabelleohneinput"><input type="text" style="width:201px" name="bezeichnung" required></td></tr>
                        <tr><td class="tabelleohneinput"><b>Geplantes Startdatum:</b></td><td class="tabelleohneinput"><input type ="date" style="width:201px" name="start" required></td></tr>
                        <tr><td class="tabelleohneinput"><b>Geplantes Enddatum:</b></td><td class="tabelleohneinput"><input type="date" name="end" style="width:201px" required></td></tr>
                        <tr><td class="tabelleohneinput"><b>Geplante Arbeitszeit (in Stunden):</b></td><td class="tabelleohneinput"><input type="number" name="arbeitszeit" min="0" style="width:201px" required></td></tr>
                        <tr><td class="tabelleohneinput"><b>Stadium:</b></td><td class="tabelleohneinput"><select name="stadium" style="width:205px" required><option value="Planung">Planung</option>
                            <option value="Realisierung">Realisierung</option><option value="Abgeschlossen">Abgeschlossen</option></select></td></tr>
                        <tr><td class="tabelleohneinput"><b>Projektleiter:</b></td><td class="tabelleohneinput"><select name="leiter" style="width:205px"><option value="0" selected="selected">Bitte Wählen </option>
                            <?php foreach($leiter as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
                        <tr><td class="tabelleohneinput"><b>Kunde:</b></td><td class="tabelleohneinput"><select name="kunde" style="width:205px" onchange="showkunde(this.value) "><option value="0" selected="selected">Bitte Wählen </option>
                            <?php foreach($kundeprojekt as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr></table>
                        <div id="kundedrop"></div>
                        <table>
                        <!--
                            <tr><td class="tabelleohneinput"> <b>Projektbeschreibung: </b></td><td class="tabelleohneinput"><input type= "textarea" form="area" cols="10" rows="10" class="textbereich" name="beschreibung" > </textarea></td></tr>
                        -->
                        <tr><td class="tabelleohneinput"><b>Management-Verweis:</b></td><td class="tabelleohneinput"><input type="text" name="jira" style="width:201px"></td></tr>
                        <tr><td class="tabelleohneinput"><b>Projektbeschreibung:</b></td><td class="tabelleohneinput"><textarea form="area" cols="10" rows="10" class="textbereich" name="beschreibung"></textarea></td></tr>
                        <tr><td class="tabelle" colspan="2"><input type="submit" class="myButton" style="width:201px" name="anlegen" value="Projekt anlegen" size="30"></td></tr>
                    </form>
                </table><br>
            </div>


            <table>
                <tr>
                    <td  class="tabelle"> 
                        <!-- Form der SortierFelder -->
                        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                            Sortierung nach Datum
                            <select name="sortieren">

                                <?php 
                                    $_SESSION['sortieren'] = $_POST['sortieren']; 

                                    if($_SESSION['sortieren']=="DESC" )
                                    {
                                        echo '<option value="'.$_SESSION['sortieren'].'" selected>Absteigend</option>';
                                        echo '<option value="ASC" > Aufsteigend </option>';
                                    }
                                    else
                                    {
                                        echo '<option value="'.$_SESSION['sortieren'].'" selected>Aufsteigend</option>';
                                        echo '<option value="DESC"> Absteigend </option>';
                                    }
                                ?>
                            </select>
                            
                    </td>
                    <td  class="tabelle">
                        <!-- Form der Zeitstatus Filter -Felder -->
                        
                            Stadium
                            <select name="status">

                            <?php
                                $_SESSION['status'] = $_POST['status'];
                                if(isset($_SESSION['status']))
                                {
                                    echo '<option value="'.$_SESSION['status'].'" selected>'.$_SESSION['status'].'</option>';

                                    if($_SESSION['status']=="Alle")
                                    {   ?>
                                            
                                            <option value="Planung">Planung</option>
                                            <option value="Realisierung">Realisierung</option>
                                            <option value="Abgeschlossen">Abgeschlossen</option>
                                        <?php
                                    }
                                    if($_SESSION['status']=="Realisierung")
                                    {   ?>
                                            <option value="Alle">Alle</option>
                                            <option value="Planung">Planung</option>
                                            <option value="Abgeschlossen">Abgeschlossen</option>
            
                                        <?php
                                    }
                                    if($_SESSION['status']=="Planung")
                                    {   ?>
                                            <option value="Alle">Alle</option>
                                            <option value="Realisierung">Realisierung</option>
                                            <option value="Abgeschlossen">Abgeschlossen</option>
                                        <?php
                                    }
                                    if($_SESSION['status']=="Abgeschlossen")
                                    {   ?>
                                            <option value="Alle">Alle</option>
                                            <option value="Planung">Planung</option>
                                            <option value="Realisierung">Realisierung</option>
                                        <?php
                                    }

                                }
                                    if(!isset($_SESSION['status']))
                                    {
                                        ?>
                                                <option value="Alle">Alle</option>
                                                <option value="Planung">Planung</option>
                                                <option value="Realisierung">Realisierung</option>
                                                <option value="Abgeschlossen">Abgeschlossen</option>
                                        <?php
                                    }
                                ?>            

                            </select>
                        
                    </td>

                    <td  class="tabelle">
                         <!-- Füllung des Kundenfilters mit verwendeten Jahren--> 
                         Kundenfilter 
                        
                            
                            <select name="suchKunde"> 
                            
                                <?php 
                                        $_SESSION['Kunde'] = $_POST['suchKunde'];
                                        $Kundeauswahl= "SELECT Firmenname FROM Projektuebersicht Where Aktiv = 1 GROUP BY Firmenname"; 

                                        $Kundenuebersicht= array();

                                        if($result = mysqli_query($db, $Kundeauswahl))
                                        {
                                            $anzahl = $result->num_rows;
                                            
                                            while($row = mysqli_fetch_assoc($result))
                                            {
                                                $Kundenuebersicht[$row['Firmenname']]=$row;
                                            }
                                        }
                                        if(isset($_SESSION['Kunde']))
                                        {
                                            echo '<option value="'.$_SESSION['Kunde'].'" selected>'.$_SESSION['Kunde'].'</option>';
                                        }
                                        if($_SESSION['Kunde'] != "Alles")
                                        {
                                                ?> <option value="Alles"> Alles   
                                                 </option>
                                                <?php
                                        }
                                                
                                            foreach($Kundenuebersicht as $row)
                                            {
                                                if($_SESSION['Kunde']==$row['Firmenname'])
                                                {
                                                    continue;
                                                }
                                                ?>                                           
                                                <option value="<?php echo ($row['Firmenname']); ?>"   
                                                ><?php echo $row['Firmenname']; ?></option>
                                                <?php
                                            }

                                        
                                    
                                ?>
                            </select> 
                        

                        <!-- Ein Submit-Button für Alles, muss im gleichen form stehen -->
                            </td>
                            <td class="tabelle">
                                    <input type="submit" value="Filtern und Sortieren"  name="filt">
                        </form>
                    </td>
                </tr>
            </table>
            <br>

        <!-- Übersichtstabelle -->
        <div class="wrapper">
        <table class="scroll" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Auftragsnummer</th>
                        <th>Projektname</th>
                        <th>Kunde</th>
                        <th>Geplantes Startdatum</th>
                        <th>Ist-Startdatum</th>
                        <th>Geplantes Enddatum</th>
                        <th>Ist-Enddatum</th>
                        <th>Stadium</th>
                        <th>Geplante Arbeitszeit (in h)</th>
                        <th>Ist-Arbeitszeit (in h)</th>
                        <th>Projektleiter</th>
                        <th>Kostenstelle</th>
                        <th>Kostenträger</th>
                        <th>Projekt-beschreibung</th>
                        <th>Management-Verweis</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php

                            // SQl Abfrage in Array speichern

                                $projekt= "SELECT * FROM Projektuebersicht Where Aktiv = 1";
                                if(isset($_POST['filt']))
                                {
                                    // KundenAlg
                                        if ($_POST['suchKunde']!= "Alles")
                                        {
                                                $_SESSION["Kunde"] = 'AND Firmenname = "'.$_POST['suchKunde'].'"';
                                        }
                                        else
                                        {
                                                $_SESSION["Kunde"] ="";
                                        }
                                    // StatusAlg
                                        if ($_POST['status']!= "Alle")
                                        {
                                                $_SESSION["status"] = 'AND Stadium = "'.$_POST['status'].'"';
                                        }
                                        else
                                        {
                                                $_SESSION["status"] ="";
                                        }

                                    // SortierAlg
                                        {
                                            if (isset($_POST['sortieren']))
                                            {
                                                    $_SESSION["sortierung"] = "ORDER BY Ist_Startdatum ".$_POST['sortieren'];

                                            }
                                            else
                                            {
                                                $_SESSION["sortierung"] = "ORDER BY Ist_Startdatum DESC";    
                                            }
                                        }

                                    $projekt= "SELECT * FROM Projektuebersicht Where (Aktiv = 1 ".$_SESSION["Kunde"]."".$_SESSION["status"].") ".$_SESSION["sortierung"]."";
                                }


                                $projektarray= array();
                                if($result = mysqli_query($db, $projekt))
                                {
                                    $anzahl = $result->num_rows;
                                    while($row = mysqli_fetch_assoc($result))
                                    {
                                        $projektarray[$row['ID']]=$row;
                                    }
                                }
                            // Array als Übersicht darstellen
                                foreach($projektarray as $value)
                                {   echo '<form action="projektdetailansicht.php" method="POST">';
                                    echo '<tr>
                                    <input type="hidden" name="ID" value="'.$value['ID'].'">
                                    <td><input type="submit" class="myButton" name="bearbeiten" value="Detailansicht"></td>
                                    <td><input type="hidden" name="Auftragsnummer" value="'.$value['Auftragsnummer'].'">'.$value['Auftragsnummer'].'</td>
                                    <td><input type="hidden" name="projektname" value="'.$value['Projektname'].'">'.$value['Projektname'].'</td>
                                    <td><input type="hidden" name="kunde" value="'.$value['Kunden_ID'].' '.$value['Firmenname'].'">'.$value['Firmenname'].'</td>
                                    <td><input type="hidden" name="gepstart" value="'.$value['gepl_Startdatum'].'">'.$value['gepl_Startdatum'].'</td>
                                    <td><input type="hidden" name="iststart" value="'.$value['ist_Startdatum'].'">'.$value['ist_Startdatum'].'</td>
                                    <td><input type="hidden" name="gepend" value="'.$value['gepl_Enddatum'].'">'.$value['gepl_Enddatum'].'</td>
                                    <td><input type="hidden" name="istend" value="'.$value['ist_Enddatum'].'">'.$value['ist_Enddatum'].'</td>
                                    <td><input type="hidden" name="stadium" value="'.$value['Stadium'].'">'.$value['Stadium'].'</td>
                                    <td align="right"><input type="hidden" name="geparbeit" value="'.$value['gepl_arbeitszeit'].'">'.number_format(floor($value['gepl_arbeitszeit']) +((fmod($value['gepl_arbeitszeit'],1)*0.6)),2,':','').'</td>
                                    <td align="right"><input type="hidden" name="istarbeit" value="'.$value['ist_arbeitszeit'].'">'.number_format(floor($value['ist_arbeitszeit']) +((fmod($value['ist_arbeitszeit'],1)*0.6)),2,':','').'</td>
                                    <td><input type="hidden" name="projektleiter" value="'.$value['Projektleiter'].'">'.$value['Projektleiter'].'</td>
                                    <td><input type="hidden" name="kostenstelle" value="'.$value['Kostenstelle'].'">'.$value['Kostenstelle'].'</td>
                                    <td><input type="hidden" name="kostentraeger" value="'.$value['Kostentraeger'].'">'.$value['Kostentraeger'].'</td>
                                    <td><input type="hidden" name="projektbesch" value="'.$value['Projektbeschreibung'].'">'.$value['Projektbeschreibung'].'</td>
                                    <td><input type="hidden" name="jira" value="'.$value['VerweisManagmentsystem'].'">'.$value['VerweisManagmentsystem'].'</td>';
                                    if ($value['Stadium'] == "Planung")
                                    {
                                        ?>
                                        <td><input type="submit" class="entfernen" name="loeschen" value="Entfernen" onclick='return confirm("Sind Sie sich sicher das Sie das Projekt löschen wollen? Alle anliegenden Arbeitspakete gehen hierbei verloren.")' formaction="projekt.php"></td>
                                        <?php
                                    }
                                    elseif($value['Stadium'] == "Realisierung") 
                                    {
                                        ?>
                                        <td><input type="submit" class="abrechnung" name="abschliessen" value="Abschließen" onclick='return confirm("Sind Sie sich sicher das Sie das Projekt Abschließen wollen? Alle anliegenden Arbeitspakete werden auch abgeschlossen.")' formaction="projekt.php"></td>
                                        <?php
                                    }
                                    else 
                                    {
                                        ?>
                                        <td></td>
                                        <?php
                                    }
                                        echo '</form>';
                                    echo'</tr>';
                                    
                                }

                        ?>
                    </tbody>
                </table>
            </div>
    </body>
</html>