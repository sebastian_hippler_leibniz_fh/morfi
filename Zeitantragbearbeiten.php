<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <Title>Zeitanträge</Title>  
        <div class="navigation"> 
            <div class="image">
                <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
            <?php
                if (isset($_POST["abmelden"]))
                {
                    session_unset();
                    session_destroy();
                    header("Location: index.php");
                    die;
                }
                $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                if ($db->connect_error)
                {
                    die("Verbindung fehlgeschlagen: " . $db->connect_error);
                }
                $nutzer = array();
                $benutzer=$_SESSION['benutzername'];
                $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                if($result = mysqli_query($db, $user))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $nutzer[]=$row['Rolle'];
                    }
                }
                if(isset($_POST['anpassen']))
                {
                    $zustand = 0;
                    $id=$_POST['id'];
                    $datumvon=$_POST['datumvon'];
                    $datumbis=$_POST['datumbis'];
                    $projektname=$_POST['projektname'];
                    $ap=$_POST['ap'];
                    $stunden = $_POST['stunden'];
                    $abwesenheit=$_POST['abwesenheit'];
                    $beschreibung=$_POST['beschreibung'];

                    $sql= $db->prepare("UPDATE arbeitszeit SET Datum = ?, DatumBis = ?, Projekt = ?, Arbeitspaket = ?, Abwesenheit = ?, Beschreibung = ?,Stunden = ?, Zeitstatus = ? WHERE ID = ?");
                    $sql->bind_param("ssiissiii", $datumvon, $datumbis, $_SESSION['Projekt_ID'], $_SESSION['AP_ID'], $abwesenheit, $beschreibung, $stunden, $zustand, $id);
                    if($sql->execute())
                    {
                        header("Location: Zeitantrag.php");
                        echo "Anpassungen vorgenommen";                        
                    }
                    else
                    {
                        echo "Es ist ein Fehler aufgetreten";
                    }

                }
                if (isset($_POST['inplanung']))
                {
                    $zustand = 0;
                    $id=$_POST['id'];

                    $sql= $db->prepare("UPDATE arbeitszeit SET Zeitstatus = ? WHERE ID = ?");
                    $sql->bind_param("ii",$zustand,  $id);

                    if($sql->execute())
                    {
                        header("Location: Zeitantrag.php");
                        echo "Anpassungen vorgenommen";
                       
                    }
                    else
                    {
                        echo "Es ist ein Fehler aufgetreten";
                    }
                }
                if(isset($_POST['verwerfen']))
                {
                    $id=$_POST['id'];
                    echo $id;
                    $sql= $db->prepare("DELETE FROM arbeitszeit WHERE ID = ?");
                    $sql->bind_param("i", $id);

                    if($sql->execute())
                    {
                        echo "Anpassungen vorgenommen";
                        header("Location: Zeitantrag.php");
                    }
                    else
                    {
                        echo "Es ist ein Fehler aufgetreten";
                    }
                }
                ?>
            <!-- SESSION initialisierung -->
                    <?php
                    
            // DB Verbindung 
                $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");;
                if ($db->connect_error)
                {
                    die("Verbindung fehlgeschlagen: " . $db->connect_error);
                }
            // SESSION initialisierung
                session_start();
                if(isset($_SESSION["Projekt_Temp"]))
                {
                    $insert=$db->prepare("DELETE FROM Projekt_Temp Where Projekt_ID = ?");
                    $insert->bind_param("s", $_SESSION["Projekt_Temp"]);
                    if($insert->execute())
                    {
                        
                        unset($_SESSION["Projekt_Temp"]);
                    }

                }
                    $_SESSION['AP_ID'];
                    $_SESSION['Projekt_ID'];
                    echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    ?>
                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>                      
                    </tr>
                </table>
            <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">
        </div>
        </form>
    </head>
    <h2><u>Zeitantrag bearbeiten</u></h2>
    <body>
        <script>
        </script>
        <div>
            <table>
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" id="area"><input type="hidden" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">
                <tr><td class="tabelle1">Datum (von):</td><td class="tabelle1"><input type="date" value="<?php if(isset($_POST['datumvon'])){echo $_POST['datumvon'];}?>" name="datumvon"></td></tr>
                <tr><td class="tabelle1">Datum (bis):</td><td class="tabelle1"><input type="date" value="<?php if(isset($_POST['datumbis'])){echo $_POST['datumbis'];}?>" name="datumbis"></td></tr>
                <tr><td class="tabelle1">Projektname:</td><td class="tabelle1"><input type="text" value="<?php if(isset($_POST['projektname'])){echo $_POST['projektname'];}?>" name="projektname"readonly></td></tr>
                <tr><td class="tabelle1">Arbeitspaket:</td><td class="tabelle1"><input type="text" value="<?php if(isset($_POST['ap'])){echo $_POST['ap'];}?>" name="ap" readonly></td></tr>
                <tr><td class="tabelle1">Abwesenheit:</td><td class="tabelle1"><input type="text" value="<?php if(isset($_POST['abwesenheit'])){echo $_POST['abwesenheit'];}?>" name="abwesenheit"></td></tr>
                <tr><td class="tabelle1">Stunden:</td><td class="tabelle1"><input type="number" value="<?php if(isset($_POST['stunden'])){echo $_POST['stunden'];}?>" name="stunden"></td></tr>
                <tr><td class="tabelle1">Beschreibung:</td><td class="tabelleohneinput"><textarea form="area" cols="10" rows="10" class="textbereich" name="beschreibung" value="<?php if(isset($_POST['beschreibung'])){echo $_POST['beschreibung'];}?>"><?php if(isset($_POST['beschreibung'])){echo $_POST['beschreibung'];}?></textarea></td></tr>
                <tr><td class="tabelle1" colspan="2"><input type="submit" name="anpassen" value="Anpassen" class="myButton">
            </form>            
                </table>
            </div>
        </div>
    </body>
</html>