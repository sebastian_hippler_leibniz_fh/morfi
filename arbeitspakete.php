<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <title>
            Arbeitspakete erstellen/bearbeiten
        </title>
        <div class="navigation">
        <div class="image">
                    <img src="images/ifano_logo_white.png" height="60" width="180">
        </div> 
            <!-- SESSION initialisierung -->
                <?php
                    session_start();
                    if (!isset($_SESSION["benutzername"]))
                    {
                        session_unset();
                        session_destroy();
                        header("Location: index.php");
                    die;
                    }
                    // Datenbank Verbindung 
                    $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                    if ($db->connect_error) 
                    {
                        die("Verbindung fehlgeschlagen: " . $db->connect_error);
                    }

                    if (isset($_POST["abmelden"]))
                    {
                        session_unset();
                        session_destroy();
                        header("Location: index.php");
                        die;
                    }
                    echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    $nutzer = array();
                    $benutzer=$_SESSION['benutzername'];
                    $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                    if($result = mysqli_query($db, $user))
                    {
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $nutzer[]=$row['Rolle'];
                        }
                    }
                           
                ?>
                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                                
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                       
                    </tr>
                </table>
                <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">
            </form>
        </div>
        <h2><u>Arbeitspaket erstellen/bearbeiten</u></h2>
        <script>
            function updateTextInput(val) 
            {
                document.getElementById('textInput').value=val; 
            }
        </script>
        <?php
                // Sessionabfragen
                    if(!isset($_SESSION["ProjektID"]))
                    {
                        echo "Bitte ein Projekt anlegen oder auswählen!";
                        echo '<button><a href="projekt.php"> OK </a></button> ';
                        die;
                    }
                    else
                    {
                        echo $_SESSION["Projektname"];
                    }

                
                //Abfrage aller Mitarbeiter
                    $query=  "SELECT * FROM mitarbeiter";
                    $leiter=array();

                    if($result = mysqli_query($db, $query))
                    {
                        while($row = mysqli_fetch_object($result))
                        {
                            $leiter[]="$row->Benutzername";
                        }
                    }
                // Abfrage Kostenstelle
                    $query= "SELECT * FROM Kostenstellen";
                    $kostenstelle = array();

                    if($result = mysqli_query($db, $query))
                    {
                        while($row = mysqli_fetch_object($result))
                        {
                            $kostenstelle[]="$row->Kostenstelle";
                        }
                    }
                // Abfrage Kostenstelle
                    $query= "SELECT * FROM Kostentraeger";
                    $kostentraeger = array();

                    if($result = mysqli_query($db, $query))
                    {
                        while($row = mysqli_fetch_object($result))
                        {
                            $kostentraeger[]="$row->Kostentraeger";
                        }
                    }
                
                // Anlegefunktion    
                    if(isset($_POST['anlegen']))
                    {
                        $insertbezeichnung=$_POST['bezeichnung'];
                        $insertgeparbeit=$_POST['geparbeit'];
                        $insertbeschreibung=$_POST['beschreibung'];
                        $insertstadium= $_POST['Stadium'];
                        $insertleiter= $_POST['leiter'];
                        $insertprojekt= $_SESSION["ProjektID"];
                        $insertstelle= $_POST['kostenstelle'];
                        $inserttraeger= $_POST['kostentraeger'];
                        $insertjira= $_POST['apjira'];
                        $Aktiv = 1;

                        $insertAP= $db->prepare("INSERT INTO arbeitspakete(Bezeichnung, geplante_Arbeitszeit, Stadium, Projekt_ID, Benutzername, Arbeitspaketbeschreibung, Kostenstelle, Kostentraeger, Aktiv, VerweisManagmentsystem)
                                                VALUES (?,?,?,?,?,?,?,?,?,?)");
                        $insertAP->bind_param("sisissssss", $insertbezeichnung, $insertgeparbeit, $insertstadium, $insertprojekt, $insertleiter, $insertbeschreibung, $insertstelle, $inserttraeger,$Aktiv, $insertjira);
                        if($insertAP->execute())
                        {
                            echo "Arbeitspaket wurde angelegt";
                            $_POST['Stadium']="";
                            $_POST['kostenstelle']="";
                            $_POST['kostentraeger']="";
                            header("Location: projektdetailansicht.php");
                        }
                        else
                        {
                            echo "Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingaben";
                        }
                    }
                
                // Bearbeitenfunktion
                    if(isset($_POST['apbearbeiten']))
                    {
                        $APID = $_SESSION["APID"];
                        $APbezeichnung= $_POST['bezeichnung'];
                        $APgeparbeit = $_POST['geparbeit'];
                        $APbeschreibung = $_POST['beschreibung'];
                        $APstadium = $_POST['Stadium'];
                        $APleiter = $_POST['leiter']; 
                        $APstelle = $_POST['kostenstelle'];
                        $APtraeger = $_POST['kostentraeger'];
                        $APjira = $_POST['apjira'];
                        

                        $updateAP= $db->prepare("UPDATE arbeitspakete SET Bezeichnung = ?, geplante_Arbeitszeit = ?, Arbeitspaketbeschreibung = ?,
                                                Stadium = ?, Benutzername= ?, Kostenstelle = ?, Kostentraeger = ?, VerweisManagmentsystem = ? WHERE ID = ?");
                        $updateAP->bind_param("sissssssi", $APbezeichnung, $APgeparbeit, $APbeschreibung, $APstadium, $APleiter, $APstelle, $APtraeger, $APjira, $APID);
                        if($updateAP->execute())
                        {
                            echo "Arbeitspaket wurde angepasst";
                        }
                        else
                        {
                            echo "Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.";
                        }
                    }
            ?>
    </head>
    <body>
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" id = "area">
        <div>
            <table>
                <tr><td class="tabelleohneinput">Bezeichnung des Arbeitspaketes: </td><td class="tabelleohneinput"><input type="text" style="width:201px" name="bezeichnung" value="<?php if(isset($_POST['Bezeichnung'])){echo $_POST['Bezeichnung'];}?>"></td></tr>
                <tr><td class="tabelleohneinput">Geplante Arbeitszeit: </td><td class="tabelleohneinput"><input type ="number" style="width:201px" name="geparbeit" min="0" value="<?php if(isset($_POST['geparbeitszeit'])){echo $_POST['geparbeitszeit'];}?>"></td></tr>
                <tr><td class="tabelleohneinput">Stadium in % (Ganze Zahlen): </td><td class="tabelleohneinput"><input type="range" style="width:201px" name="Stadium" onchange="updateTextInput(this.value)" min = "0" max = "100" step="5" value="<?php if(isset($_POST['Stadium'])){echo $_POST['Stadium'];}?>">
                <input type="text" id="textInput" value="<?php if(isset($_POST['Stadium'])){echo $_POST['Stadium'];} ?>" placeholder="0" style="width:20px" readonly name="Stadium"></td></tr>
                <tr><td class="tabelleohneinput">Bearbeitender Mitarbeiter:</td><td class="tabelleohneinput"><select name="leiter" style="width:201px">
                    <option value="<?php if (isset($_POST['Benutzername'])){echo $_POST['Benutzername'];}?>" selected="selected"><?php if(isset($_POST['Benutzername'])){ echo $_POST['Benutzername'];}?></option>
                    <?php foreach($leiter as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
                <tr><td class="tabelleohneinput">Kostenstelle:</td><td class="tabelleohneinput"><select name="kostenstelle" style="width:201px">
                    <option value="<?php if (isset($_POST['kostenstelle'])){echo $_POST['kostenstelle'];}?>" selected="selected"><?php if(isset($_POST['kostenstelle'])){ echo $_POST['kostenstelle'];}?></option>
                    <?php foreach($kostenstelle as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
                <tr><td class="tabelleohneinput">Kostenträger:</td><td class="tabelleohneinput"><select name="kostentraeger" style="width:201px">
                    <option value="<?php if (isset($_POST['kostentraeger'])){echo $_POST['kostentraeger'];}?>" selected="selected"><?php if(isset($_POST['kostentraeger'])){ echo $_POST['kostentraeger'];}?></option>
                    <?php foreach($kostentraeger as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
                <tr><td class="tabelleohneinput">Arbeitspaketbeschreibung: </td><td class="tabelleohneinput"><textarea form= "area" cols="10" rows="10" class="textbereich" name="beschreibung" value="<?php if(isset($_POST['Arbeitspaketbeschreibung'])){echo  $_POST['Arbeitspaketbeschreibung'];}?>"><?php if(isset($_POST['Arbeitspaketbeschreibung'])){echo  $_POST['Arbeitspaketbeschreibung'];}?></textarea></td></tr>
                <tr><td class="tabelleohneinput">Management-Verweis: </td><td class="tabelleohneinput"><input type ="text" style="width:201px" name="apjira" value="<?php if(isset($_POST['kuerzeljira'])){echo $_POST['kuerzeljira'];}?>"></td></tr>
                    <?php
                        if(isset($_POST['apanpassen']))
                            {
                                echo '<tr><td class="tabelle" colspan="2"><input type="submit" style="width:201px" class="myButton" name="apbearbeiten" value="Arbeitspaket anpassen"></td></tr></table>';
                            }
                        else
                            {
                                echo '<tr><td class="tabelle" colspan="2"><input type="submit" class="myButton" name="anlegen" Value="Arbeitspaket anlegen"></td></tr></table>';
                            }
                    ?>
                    </form>
    </body>
</html>