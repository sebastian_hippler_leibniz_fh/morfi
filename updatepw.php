<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <Title>Passwort ändern</Title>  
        <div class="navigation"> 
            <div class="image">
                <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
            <!-- SESSION initialisierung -->
                    <?php

            // DB Verbindung 
                $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");;
                if ($db->connect_error)
                {
                    die("Verbindung fehlgeschlagen: " . $db->connect_error);
                }
            // SESSION initialisierung
                session_start();
                if(isset($_SESSION["Projekt_Temp"]))
                {
                    $insert=$db->prepare("DELETE FROM Projekt_Temp Where Projekt_ID = ?");
                    $pimmel=  $_SESSION["Projekt_Temp"];
                    $insert->bind_param("s", $_SESSION["Projekt_Temp"]);
                    if($insert->execute())
                    {
                        
                        unset($_SESSION["Projekt_Temp"]);
                    }

                }
                $nutzer = array();
                $benutzer=$_SESSION['benutzername'];
                $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                if($result = mysqli_query($db, $user))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $nutzer[]=$row['Rolle'];
                    }
                }
                    if (isset($_POST["abmelden"]))
                    {
                        session_unset();
                        session_destroy();
                        header("Location: index.php");
                        die;
                    }
                    echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    
                    ?>
                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                       
                    </tr>
                </table>
            <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">
        </div>
    </head>
    <h2><u>Passwort ändern</u></h2>
    <body>
        <script>
            function myFunction() 
            {
                var x = document.getElementById("mypassword");
                if (x.type === "password") 
                {
                    x.type = "text";
                } 
                else 
                {
                    x.type = "password";
                }
            } 
        </script>
        <div>
            <table>
                <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
                    <tr><td class="tabelleohneinput">Neues Passwort eingeben:</td><td class="tabelleohneinput"><input type="password" name ="passwort" id="mypassword"></td></tr>
                    <tr><td class="tabelleohneinput" colspan="2"><input type="checkbox"onclick="myFunction()">Passwort anzeigen</td></tr>
                    <tr><td colspan="2" class="tabelle"><input type="submit" class="myButton" name="anmelden" value="Anlegen"></td></tr>
                </form>            
            </table>
            <?php

                if (isset($_POST['anmelden']))
                {
                    
                    // SESSION initialisierung 
                        $passwort= $_POST['passwort'];

                    // DB Verbindung und Passwort abfrage
                        $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                        if ($db->connect_error) 
                        {
                            die("Verbindung fehlgeschlagen: " . $db->connect_error);
                        }
                        $query= $db->prepare("UPDATE mitarbeiter SET Passwort = ? WHERE Benutzername = ?");
                        $passwort = PASSWORD_HASH($passwort, PASSWORD_BCRYPT);
                        $query->bind_param("ss", $passwort, $_SESSION["benutzername"]);
                        if ($query->execute()) 
                        {
                            echo "Passwort geändert.";
                        } 
                        else
                        {
                            echo "Änderung Fehlgeschlagen.";
                        }
                }
            ?>
        </div>
    </body>
</html>