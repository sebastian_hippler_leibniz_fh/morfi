<head>
    <link rel="stylesheet" href="style.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
</head>
<body>
<?php
$db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
if ($db->connect_error)
{
    die("Verbindung fehlgeschlagen: " . $db->connect_error);
}
$arbeitszeit = "SELECT * FROM arbeitszeit";
if($result = mysqli_query($db, $arbeitszeit))
{
    while($row = mysqli_fetch_assoc($result))
    {
        $allezeiten[$row['Datum']]=$row['Projekt'];
    }
}
if(isset($_GET['month']))
        $time = mktime(12,0,0,$_GET['month'],1,$_GET['year']);
    else 
        $time = mktime(12,0,0,date('n'),1,date('Y'));
    
$monate = array(1=>"Januar", 
                2=>"Februar", 
                3=>"März", 
                4=>"April", 
                5=>"Mai", 
                6=>"Juni", 
                7=>"Juli", 
                8=>"August", 
                9=>"September", 
                10=>"Oktober", 
                11=>"November", 
                12=>"Dezember");
$monat = date("n", $time);
$jahr = date("Y", $time);

?>
<a href="?month=<?php if($monat==1) echo 12; else echo $monat-1; ?>&year=<?php if($monat==1) echo $jahr-1; else echo $jahr; ?>" >&LT;</a>
<?php
echo $monate[$monat].' ';
echo $jahr;
?>
<a href="?month=<?php if($monat==12) echo 1; else echo $monat+1; ?>&year=<?php if($monat==12) echo $jahr+1; else echo $jahr; ?>" >&GT;</a>

<table>
    <thead>
        <tr>
            <th>Mo</th>
            <th>Di</th>
            <th>Mi</th>
            <th>Do</th>
            <th>Fr</th>
            <th>Sa</th>
            <th>So</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php 
            $springer = $time - (date('N', $time)-1)*3600*24;
            $monatbeenden = true;
            while (true) 
            {
                echo '<td>'.date('j', $springer).'</td>';
                if(date('N', $springer) == 7)
                    {
                        if(!$monatbeenden) break;
                        echo '</tr><tr>';
                    }
                $springer += 60*60*24;
                echo date("Y-m-d", strtotime($springer))."<br>";
                if(date('n', $springer) > date('n', $time) && date('n', $time) != 1 || date('n', $springer) == 2 && date('n', $time) == 1 || date('Y', $springer) > date('Y', $time))
                {
                    $monatbeenden = false;
                    if(date('N', $springer) == 1) break;
                }
            }
            ?>
        </tr>
    </tbody>
</table>
    </div>
</body>