<!DOCTYPE HTML>
<html>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes"/>
        <link rel="stylesheet" href="style.css">
        <Title>Anmeldung IFANO Ressourcenmanagement</Title>
        <div class="navigationanmeldung">
        <img src="images/ifano_logo_white.png" height="60" width="180" class="image">
        </div>
    </head>
    <h1> <align="center"><u>ANMELDUNG IFANO Ressourcenmanagement Mobile Ansicht</u></h1>
    <body>
        <div>
            <form action="<?php $_SERVER['PHP_SELF']?>" method="POST">
                <table> <align="center">
                    <tr><td class="tabellemobil">Benutzername:</td><td class="tabellemobil"><input type= "text" name="benutzername" ></td></tr>
                    <tr><td class="tabellemobil">Passwort: </td><td class="tabellemobil"><input type="password" name ="passwort"></td></tr>
                    <tr><td colspan="2" class="tabelle"><input type="submit" name="anmelden" class="myButton" value="Anmelden"
                            colspan="2" class="tabelle"><input type="submit" name="desktop" class="myButton" value="Zur Desktop Ansicht wechseln"></td></tr>

                </table>
            </form>


            <?php
                // Anmeldung und DB Vergleich
                    // DB Verbindung
                        $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                        if ($db->connect_error)
                        {
                            die("Verbindung fehlgeschlagen: " . $db->connect_error);
                        }
                    // Anmeldung
                        if (isset($_POST['anmelden']))
                        {
                            $benutzername= $_POST['benutzername'];
                            $benutzername= strtolower($benutzername);
                            $benutzername= ucwords($benutzername);
                            $passwort= $_POST['passwort'];
                        }
                    // DB Vergleich mit Eingabe
                    if(isset($_POST['anmelden']))
                    {
                        $stmt = $db->prepare("SELECT * FROM mitarbeiter WHERE benutzername = ?");
                        $stmt->bind_param("s", $benutzername);
                        $stmt->execute();
                        $user = $stmt->get_result()->fetch_assoc();

                        if ($user && password_verify($passwort, $user['Passwort']))
                        {
                            session_start();
                            $_SESSION["ID"]= $user["ID"];
                            $_SESSION["benutzername"]= $benutzername;
                            //header("Location:http://Ifano.Online/MORFI/mobilezeiterfassung.php");
                            header("Location: mobilezeiterfassung.php");
                        }
                        else
                        {
                            echo "Benutzername oder Passwort nicht korrekt!";
                        }
                    }
                // Buttonvergabe
                    if(isset($_POST['desktop']))
                    {
                    //header("Location:http://Ifano.Online/MORFI/index.php");
                    header("Location:http: index.php");
                    }

                    if(isset($_POST['abmelden']))
                    {
                        echo "Sie haben sich erfolgreich abgemeldet!";
                    }
            ?>
        </div>
    </body>
</html>
