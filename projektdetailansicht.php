<!DOCTYPE HTML>
<html>
    <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="style.css">
            <div class="navigation">
                <div class="image">
                        <img src="images/ifano_logo_white.png" height="60" width="180">
                </div>
                <!-- SESSION initialisierung -->
                    <?php
                        session_start();
                        
                        if (isset($_POST["abmelden"]))
                        {
                            session_unset();
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }
                // DB Verbindung
                    $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                    if ($db->connect_error) 
                    {
                        die("Verbindung fehlgeschlagen: " . $db->connect_error);
                    }
                    $nutzer = array();
                    $benutzer=$_SESSION['benutzername'];
                    $user = "SELECT Rolle FROM mitarbeiter WHERE Benutzername = '$benutzer'";
                    if($result = mysqli_query($db, $user))
                    {
                        while($row = mysqli_fetch_assoc($result))
                        {
                            $nutzer[]=$row['Rolle'];
                        }
                    }

                        if (isset($_POST['bearbeiten'])|| isset($_POST['anlegen']) )
                        {
                            $_SESSION["ProjektID"] = $_POST['ID'];
                            $_SESSION["Projektname"] = $_POST['projektname'];
                            $_SESSION["Auftragsnummer"] = $_POST['Auftragsnummer'];
                            $_SESSION["gepl_startdatum"] = $_POST['gepstart'];
                            $_SESSION["gepl_enddatum"] = $_POST['gepend'];
                            $_SESSION["gepl_arbeitszeit"] = $_POST['geparbeit'];
                            $_SESSION["Projektleiter"] = $_POST['projektleiter'];
                            $_SESSION["Stadium"] = $_POST['stadium'];
                            $_SESSION["jira"] = $_POST['jira'];
                            $_SESSION["Kostenträger"]= $_POST['kostentraeger'];
                            $_SESSION["Kostenstelle"]= $_POST['kostenstelle'];
                            $_SESSION["Kunden_ID"] = $_POST['kunde'];



                            $fuellen=  $db -> prepare("SELECT * FROM  Projekt_Temp Where Projekt_ID = ?");
                            $fuellen->bind_param("s", $_SESSION["ProjektID"]);
                            $fuellen->execute();
                            $result = $fuellen->get_result();
                            $Projekt_Temp=array();

                            while($row = mysqli_fetch_assoc($result))
                            {

                                $Projekt_Temp[]=$row;

                            }

                        

                            if(isset($Projekt_Temp[0]))
                            {
                                header("Location: projekt.php");
                            }
                            else
                            {
                                $insert=$db->prepare("INSERT INTO Projekt_Temp (Projekt_ID, Benutzer_ID)
                                VALUES(?,?)");
                                $insert->bind_param("ss", $_SESSION["ProjektID"], $_SESSION["ID"]);
                                if($insert->execute())
                                {

                                }
                                else
                                {

                                }
                                $_SESSION["Projekt_Temp"]=$_SESSION["ProjektID"];

                            }
                        
                        }
                        echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                    ?>
                <table>
                    <tr>
                        <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">
                            <td class = "menuborderless"><a href="hauptseite.php" class="inputbutton"> Home</a></td>
                           <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->
                            <td class = "menuborderless">
                            <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                <option class="inputbutton">
                                    Zeitmanagement
                                </option>
                                <option value ="Gesamtarbeitszeit.php">Gesamtarbeitzeit</option>
                                <option value ="Arbeitszeiterfassung.php">Arbeitszeiterfassung</option>
                                <option value ="Zeitantrag.php">Zeitanträge</option>
                                <?php
                                if($nutzer[0] == "Supervisor")
                                {
                                    echo '<option value="Allezeiten.php"> Alle Arbeitszeiten </option>';
                                }
                                ?>
                            </select></td>
                            <td class = "menuborderless"><a href="projekt.php" class="inputbutton">Projekte</a></td>
                            <td class = "menuborderless"><a href="Nutzerinsert.php" class="inputbutton">Benutzer anlegen</a></td>
                            <td class = "menuborderless"><a href="kundenmaske.php" class="inputbutton">Kunde anlegen</a></td>
                            <td class = "menuborderless">
                                <select class="inputbuttonweiteres" onChange="window.location.href=this.value">
                                    <option class="inputbutton">
                                        Weiteres
                                    </option>
                                    <option value ="updatepw.php">Passwort ändern</option>
                                </select>
                            </td>
                       
                    </tr>
                </table>
                <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">  
                </form> 
            </div>
        <title>
            Detailansicht
        </title>
        <h2><u>Projekt bearbeiten</u></h2>
    </head>

    <body onbeforeunload = "return ende()">
        <script>
            function ende(str) 
            {

                        if (window.XMLHttpRequest) 
                        {
                            xmlhttp = new XMLHttpRequest();
                        } 
                        else 
                        {
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function() 
                        {
                            if (this.readyState == 4 && this.status == 200) 
                            {
                                document.getElementById("kundedrop").innerHTML = this.responseText;
                            }
                        };
                        xmlhttp.open("GET","AjaxProjektBlock.php?q="+str, true);
                        xmlhttp.send();
            }
        </script>
    </body>

    <body>
        <script>
                // Funktion löschabfrage
                    function popup() 
                    {
                        var txt;
                        var r = confirm("Soll das Arbeitspaket wirklich gelöscht werden?");
                        if (r == true) {
                            txt = true;
                            return true;
                        } else {
                            txt = false;
                            return false;
                        }
                        document.getElementById("demo").innerHTML = txt;
                    }
        </script>
        <?php

                $select=$db->prepare("SELECT * FROM projekt WHERE Projektname = ?");

                $select->bind_param("s", $_SESSION['projektname']);
                $select->execute();
                $result = $select->get_result();
                while($row = $result->fetch_assoc())
                {
                    $_SESSION["ProjektID"] = $row['ID'];
                    $_SESSION["Auftragsnummer"] = $row['Auftragsnummer']; 
                    $_SESSION["Projektname"] = $row['Projektname'];
                    $_SESSION["Kunden_ID"] = $row['Kunden_ID'];
                    $_SESSION["gepl_startdatum"] = $row['gepl_Startdatum'];
                    $_SESSION["ist_startdatum"] = $row['ist_Startdatum'];
                    $_SESSION["gepl_enddatum"] = $row['gepl_Enddatum'];
                    $_SESSION["ist_enddatum"] = $row['ist_Enddatum'];
                    $_SESSION["stadium"] = $row['Stadium'];
                    $_SESSION["gepl_arbeitszeit"] = $row['gepl_arbeitszeit'];
                    $_SESSION["ist_arbeitszeit"] = $row['ist_arbeitszeit'];
                    $_SESSION["Projektleiter"] = $row['Projektleiter'];
                    $_SESSION["Projektbeschreibung"] = $row['Projektbeschreibung'];
                    $_SESSION["Kostenstelle"] = $row['Kostenstelle'];
                    $_SESSION["Kostenträger"] = $row['Kostentraeger'];
                    $_SESSION["jira"] = $row['VerweisManagmentsystem'];
                }
            //Abfrage alle Kunden
                $kunde=  "SELECT * FROM kunde";
                $kundeprojekt=array();
                $IDprojekt=array();

                if($result = mysqli_query($db, $kunde))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $kundeprojekt["ID"][]="$row->ID";
                        $kundeprojekt["Firmenname"][] = " $row->Firmenname";
                    }
                }
                
            //Abfrage alle Mitarbeiter 
                $query=  "SELECT * FROM mitarbeiter";
                $leiter=array();
                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $leiter[]="$row->Benutzername";
                    }
                }
                
                $zwischenkunde = array();
                $zwischenkunde = explode(" ", $_SESSION["Kunden_ID"]);
                $kundenzahl = $zwischenkunde[0]-1;
                $endkunde = $kundeprojekt["ID"][$kundenzahl];
            // Abfrage alle Kostenstellen
                $query=  "SELECT * FROM Kostenstellen";
                $stelle=array();
                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $stelle[]="$row->Kostenstelle";
                    }
                }
            // Abfrage alle Kostenträger
                $query=  "SELECT * FROM Kostentraeger";
                $traeger=array();
                if($result = mysqli_query($db, $query))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $traeger[]="$row->Kostentraeger";
                    }
                }
               $stadium=$_SESSION["Stadium"];
            // Anlegefunktion
             /*   if (isset($_POST['anlegen']))
                {
                    $bezeichnung=$_POST['bezeichnung'];
                    $gepstart=$_POST['start'];
                    $gepend=$_POST['end'];
                    $arbeitszeit=$_POST['arbeitszeit'];
                    $beschreibung=$_POST['beschreibung'];
                    $projektleiter=$_POST['leiter'];
                    $projektkunde=$_POST['kunde'];
                    $stadium=$_POST['stadium'];
                    $auftragsnummer = $_POST['auftragsnummer'];
                    $Kostenstelle =  $_SESSION['kostenstelle'];
                    $Kostentraeger =  $_SESSION['kostentraeger'];
                    $jira = $_POST['jira'];
                    $Aktiv = 1;
                    $_SESSION["Projektname"] = $bezeichnung;
                    $_SESSION["Auftragsnummer"] = $auftragsnummer;
                    $_SESSION["gepl_startdatum"] = $gepstart;
                    $_SESSION["gepl_enddatum"] = $gepend;
                    $_SESSION["gepl_arbeitszeit"] = $arbeitszeit;
                    $_SESSION["Projektleiter"] = $projektleiter;
                    $_SESSION["Stadium"] = $stadium;
                    $_SESSION["jira"] = $jira;
                    $_SESSION["Kostenträger"]=$Kostentraeger;
                    $_SESSION["Kostenstelle"]=$Kostenstelle;
                    $_SESSION["Kunden_ID"] = $projektkunde;


                    $insert=$db->prepare("INSERT INTO projekt (Auftragsnummer, Projektname, Kunden_ID, gepl_Startdatum,
                                        gepl_Enddatum, gepl_arbeitszeit, Projektleiter, Projektbeschreibung, Stadium, Kostenstelle, Kostentraeger, Aktiv, VerweisManagmentsystem)
                                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    $insert->bind_param("sssssssssssss", $auftragsnummer, $bezeichnung, $projektkunde,
                                        $gepstart, $gepend, $arbeitszeit, $projektleiter,
                                        $beschreibung, $stadium, $Kostenstelle, $Kostentraeger, $Aktiv, $jira);
                    if($insert->execute())
                    {
                        echo "Projekt angelegt!";
                        $select=$db->prepare("SELECT * FROM projekt WHERE Projektname = ?");
                        $select->bind_param("s", $bezeichnung);
                        $select->execute();
                        $result = $select->get_result();
                        while($row = $result->fetch_assoc())
                        {
                            $_SESSION["ProjektID"] = $row['ID'];
                        }
                    }
                    else
                    {
                        echo "Projektanlage fehlgeschlagen!";
                    }
                }*/
            // Beaebeitefunktion
                if(isset($_POST['bearbeiten']))
                {
                    $bezeichnung=$_POST['projektname'];
                    $gepstart=$_POST['gepstart'];
                    $gepend=$_POST['gepend'];
                    $arbeitszeit=$_POST['geparbeit'];
                    $beschreibung=$_POST['projektbesch'];
                    $projektleiter=$_POST['projektleiter'];
                    $projektkunde=$_POST['kunde'];
                    $Kostenstelle =  $_POST['kostenstelle'];
                    $Kostentraeger =  $_POST['kostentraeger'];
                    $Jira = $_POST['jira'];
                    $_SESSION["Projektname"] = $_POST['projektname'];
                    $_SESSION["Auftragsnummer"] = $_POST['Auftragsnummer'];
                    $_SESSION["gepl_startdatum"] = $_POST['gepstart'];
                    $_SESSION["gepl_enddatum"] = $_POST['gepend'];
                    $_SESSION["gepl_arbeitszeit"] = $_POST['geparbeit'];
                    $_SESSION["Projektleiter"] = $_POST['projektleiter'];
                    $_SESSION["Stadium"] = $_POST['stadium'];
                    $_SESSION["jira"] = $_POST['jira'];
                    $_SESSION["Kostenträger"]= $_POST['kostentraeger'];
                    $_SESSION["Kostenstelle"]= $_POST['kostenstelle'];
                    $_SESSION["Kunden_ID"] = $_POST['kunde'];
                    $_SESSION["Projektbeschreibung"] = $_POST['projektbesch'];
                }

            // Anpassfuntkion   
                if(isset($_POST['anpassen']))
                {
                    $projektbez=$_POST['projektbez'];
                    $gepstart=$_POST['gepstart'];
                    $gepend=$_POST['gepend'];
                    $arbzeit=$_POST['arbzeit'];
                    $probesch=$_POST['probesch'];
                    $projektleiter=$_POST['projektleiter'];
                    $auftragsnummer=$_POST['Auftragsnummer'];
                    $kundenup = $_POST['kundennummer'];
                    $stadiumup = $_POST['stadium'];
                    $Kostenstelle =  $_POST['kostenstelle'];
                    $Kostentraeger =  $_POST['kostentraeger'];
                    $jira = $_POST['jira'];

                    $update=$db->prepare("UPDATE projekt SET Auftragsnummer = ?, Projektname = ?, gepl_Startdatum = ?, gepl_Enddatum = ?, gepl_arbeitszeit = ?, Projektbeschreibung = ?, Projektleiter = ?, Kunden_ID = ?, Stadium = ?, Kostenstelle=?, Kostentraeger=?, VerweisManagmentsystem=?
                                        WHERE ID =".$_SESSION['ProjektID']."");
                    $update->bind_param("ssssssssssss",$auftragsnummer, $projektbez, $gepstart, $gepend, $arbzeit, $probesch, $projektleiter, $kundenup, $stadiumup,$Kostenstelle,$Kostentraeger,$jira);
                    if ($update->execute())
                    {
                        echo "Projekt wurde angepasst";
                        $_SESSION["Auftragsnummer"] = $auftragsnummer;
                        $_SESSION["Projektname"] = $projektbez;
                        $endkunde = $kundenup;
                        $_SESSION["gepl_startdatum"] = $gepstart;
                        $_SESSION["gepl_enddatum"] = $gepend;
                        $_SESSION["stadium"] = $stadiumup;
                        $_SESSION["Projektleiter"] = $projektleiter;
                        $_SESSION["Projektbeschreibung"] = $probesch;
                        $_SESSION["Kostenstelle"] = $Kostenstelle;
                        $_SESSION["Kostentraeger"] = $Kostentraeger;
                        $_SESSION["jira"] = $jira;
                    }
                    else
                    {
                        echo "Projekt wurde nicht angepasst";
                    }
                }
            // AP Löschfuntkion
                    if(isset($_POST['Aploeschen']))
                    {
                        if($_POST['Aploeschen']==true)
                            {
                            $deleteAPID=$_POST['ID'];
                            $inaktiv ="UPDATE arbeitspakete Set Aktiv  = 0 WHERE ID = ?";
                            $deleteAP=$db->prepare($inaktiv);
                            $deleteAP-> bind_param("i", $deleteAPID);

                            if($deleteAP->execute())
                            {
                                echo "Arbeitspaket wurde gelöscht";
                            }
                            else
                            {
                                echo "Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.";
                            }

                        }
                    }

                    if(isset($_POST['abrechnen']))
                    {
                        if($_POST['abrechnen']==true)
                            {
                            $deleteAPID=$_POST['ID'];
                            $inaktiv ="UPDATE arbeitspakete Set Aktiv  = 0 WHERE ID = ?";
                            $deleteAP=$db->prepare($inaktiv);
                            $deleteAP-> bind_param("i", $deleteAPID);

                            if($deleteAP->execute())
                            {
                                echo "Arbeitspaket wurde abgeschlossen";
                            }
                            else
                            {
                                echo "Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.";
                            }

                        }
                    }
        ?>
        <div>
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" id="area">
        <table>
            <tr><td class="tabelleohneinput"> Bezeichnung: </td><td class="tabelleohneinput"><input type="text" style="width:201px" name="projektbez" value="<?php if(isset($_SESSION["Projektname"])){echo $_SESSION["Projektname"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Auftragsnummer: </td><td class="tabelleohneinput"><input type="text" style="width:201px" name="Auftragsnummer" value="<?php if(isset($_SESSION["Auftragsnummer"])){echo $_SESSION["Auftragsnummer"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Geplantes Startdatum: </td><td class="tabelleohneinput"><input type="date" style="width:201px" name="gepstart" value="<?php if(isset($_SESSION["gepl_startdatum"])){echo $_SESSION["gepl_startdatum"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Geplantes Enddatum: </td><td class="tabelleohneinput"><input type="date" style="width:201px" name="gepend" value="<?php if(isset( $_SESSION["gepl_enddatum"])){echo  $_SESSION["gepl_enddatum"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Geplante Arbeitszeit (in Stunden): </td><td class="tabelleohneinput"><input type="number" style="width:201px" name="arbzeit" value="<?php if(isset($_SESSION["gepl_arbeitszeit"])){echo $_SESSION["gepl_arbeitszeit"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Projektleiter: </td><td class="tabelleohneinput"><select name="projektleiter" style="width:207px"><option value="<?php echo $_SESSION["Projektleiter"]; ?>" selected><?php echo $_SESSION["Projektleiter"]; ?></option>
            <?php
                foreach($leiter as $index => $value)
                {
                    if($_SESSION["Projektleiter"]!=$value)
                    {
                        echo '<option value ="'.$value.'">'.$value.'</option>';
                    }
                }
            ?>
            <tr><td class="tabelleohneinput"> Kunde: </td><td class="tabelleohneinput"><select name="kundennummer" style ="width:207px"><option value ="<?php echo $endkunde;?>" selected><?php echo $kundeprojekt["ID"][$endkunde-1].$kundeprojekt["Firmenname"][$endkunde-1];?></option>
            <?php 
                foreach($kundeprojekt as $index => $inneres_array)
                {
                    for($i = 0;$i < count($inneres_array); $i++)
                    {
                        if($inneres_array[$i]!=$endkunde)
                        {
                            echo '<option value ="'.$inneres_array[$i].'">'.$inneres_array[$i].$kundeprojekt["Firmenname"][$i].'</option>';
                        }
                    }
                break;
                }
            ?>
            </select>
            </td></tr>
            <tr><td class="tabelleohneinput"> Stadium: </td><td class="tabelleohneinput"><select name="stadium" style="width:207px">
            <option value ="<?php if(isset($_SESSION["Stadium"]))
            {
                switch($_SESSION["Stadium"])
                {
                    case "Planung":
                    echo $_SESSION["Stadium"].'">Planung</option><option value="Realisierung">Realisierung</option><option value="Abgeschlossen">Abgeschlossen</option>';
                    break;

                    case"Realisierung":
                    echo $_SESSION["Stadium"].'">Realisierung</option><option value="Planung">Planung</option><option value="Abgeschlossen">Abgeschlossen</option>';
                    break;

                    case"Abgeschlossen":
                    echo $_SESSION["Stadium"].'">Abgeschlossen</option><option value="Planung">Planung</option><option value="Realisierung">Realisierung</option>';
                    break;

                    default:
                    die;    
                }
            }
            ?>">
            </td></tr>

            <tr><td class="tabelleohneinput"><b>Kostenstelle:</b></td><td class="tabelleohneinput"><select name="kostenstelle" style="width:201px">
                <option value="<?php if (isset($_SESSION['Kostenstelle'])){echo $_SESSION['Kostenstelle'];}?>" selected="selected"><?php if(isset($_SESSION['Kostenstelle'])){ echo $_SESSION['Kostenstelle'];}?></option>
                <?php foreach($stelle as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
            <tr><td class="tabelleohneinput"><b>Kostenträger:</b></td><td class="tabelleohneinput"><select name="kostentraeger" style="width:201px">
                <option value="<?php if (isset($_SESSION['Kostenträger'])){echo $_SESSION['Kostenträger'];}?>" selected="selected"><?php if(isset($_SESSION['Kostenträger'])){ echo $_SESSION['Kostenträger'];}?></option>
                <?php foreach($traeger as $value){echo '<option value="'.$value.'">'.$value.'</option>';}?></select></td></tr>
                <tr><td class="tabelleohneinput"> Management-Verweis: </td><td class="tabelleohneinput"><input type="text" style="width:201px" name="jira" value="<?php if(isset($_SESSION["jira"])){echo $_SESSION["jira"];}?>"></td></tr>
            <tr><td class="tabelleohneinput"> Projektbeschreibung: </td><td class="tabelleohneinput"><textarea form="area" cols="10" rows="10" class="textbereich" name="probesch" value="<?php if(isset($_SESSION["Projektbeschreibung"])){echo $_SESSION["Projektbeschreibung"];}?>"><?php if(isset($_SESSION["Projektbeschreibung"])){echo $_SESSION["Projektbeschreibung"];}?></textarea></td></tr>
            <tr><td class="tabelle"><input type="submit" class="myButton" name="APanlegen" value="Arbeitspaket anlegen" formaction="arbeitspakete.php"></td>
            <td class="tabelle"><input type="submit" class="myButton" name="anpassen" value="Projekt anpassen"></td></tr>
        </table>
        <br>
        </form>
        </div>

        
        <table>
                <tr>
                    <td> 
                        <!-- Form der SortierFelder -->
                        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                            Sortierung nach ID
                            <select name="sortieren">

                                <?php 
                                    $_SESSION['sortieren'] = $_POST['sortieren']; 

                                    if($_SESSION['sortieren']=="DESC" )
                                    {
                                        echo '<option value="'.$_SESSION['sortieren'].'" selected>Absteigend</option>';
                                        echo '<option value="ASC" > Aufsteigend </option>';
                                    }
                                    else
                                    {
                                        echo '<option value="'.$_SESSION['sortieren'].'" selected>Aufsteigend</option>';
                                        echo '<option value="DESC"> Absteigend </option>';
                                    }
                                ?>
                            </select>
                            
                    </td>
                    <td>
                        <!-- Form der Zeitstatus Filter -Felder -->
                        
                            Stadium
                            <select name="status">
                                <?php 
                                    $_SESSION['status'] = $_POST['status']; 
                                ?>

                                <option value="Alle"           
                                <?php 
                                        if($_SESSION['status'] == "Alle") 
                                        echo "selected='selected'";
                                ?>
                                >Alle</option>
                                <option value="<=30"          
                                <?php 
                                        if($_SESSION['status'] == "<=30") 
                                        echo "selected='selected'";
                                ?>
                                ><=30</option>
                                <option value="<=60"  
                                <?php 
                                        if($_SESSION['status'] == "<=60") 
                                            echo "selected='selected'";
                                ?>
                                ><=60</option>
                                <option value="=100"         
                                <?php 
                                        if($_SESSION['status'] == "Abgeschlossen") 
                                            echo "selected='selected'";
                                ?>
                                >Abgeschlossen</option>


                            </select>
                        
                    </td>

                    <td>
                         <!-- Füllung des Kundenfilters mit verwendeten Jahren--> 
                         Benutzerfilter 
                        <br>
                        
                            
                            <select name="SuchBenutzer"> 
                            
                                <?php 
                                        $_SESSION['SuchBenutzer'] = $_POST['SuchBenutzer'];
                                        $Benutzerauswahl= "SELECT Benutzername FROM arbeitspakete Where Aktiv = 1 GROUP BY Benutzername"; 

                                        $Benutzeruebersicht= array();

                                        if($result = mysqli_query($db, $Benutzerauswahl))
                                        {
                                            $anzahl = $result->num_rows;
                                            
                                            while($row = mysqli_fetch_assoc($result))
                                            {
                                                $Benutzeruebersicht[]=$row;
                                            }
                                        }
                                        if(isset($_SESSION['SuchBenutzer']))
                                        {
                                            echo '<option value="'.$_SESSION['SuchBenutzer'].'" selected>'.$_SESSION['SuchBenutzer'].'</option>';
                                        }
                                        if($_SESSION['SuchBenutzer'] != "Alles")
                                        {
                                                ?> <option value="Alles"> Alles   
                                                 </option>
                                                <?php
                                        }
                                                
                                            foreach($Benutzeruebersicht as $row)
                                            {
                                                if($_SESSION['SuchBenutzer']==$row['Benutzername'])
                                                {
                                                    continue;
                                                }
                                                ?>                                           
                                                <option value="<?php echo ($row['Benutzername']); ?>"   
                                                ><?php echo $row['Benutzername']; ?></option>
                                                <?php
                                            }

                                        

                                ?>
                            </select> 
                        <br> 

                        <!-- Ein Submit-Button für Alles, muss im gleichen form stehen -->
                        <input type="submit" value="Filtern und Sortieren"  name="filt">
                        </form>
                    </td>
                </tr>
            </table>
        <div class="wrapper">
        <table class="scroll" cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th></th>    
                        <th>Arbeitspaket-ID</th>
                        <th>AP-Name</th>
                        <th>Geplante Arbeitszeit</th>
                        <th>Ist Arbeitszeit</th>
                        <th>Stadium</th>
                        <th>Bearbeiter</th>
                        <th>Beschreibung</th>
                        <th>Kostenstelle</th>
                        <th>Kostenträger</th>
                        <th>Management-Verweis</th>
                        <th></th>  
                    </tr>
                    <tbody>
                        <?php
                            // SQl Abfrage in Array speichern

                            $user= "SELECT * from arbeitspakete WHERE (Projekt_ID ='".$_SESSION["ProjektID"]."' AND Aktiv = 1 AND NOT Bezeichnung ='Urlaub' AND NOT Bezeichnung = 'Krankheit')";
                                if(isset($_POST['filt']))
                                {
                                    // KundenAlg
                                        if ($_POST['SuchBenutzer'] != "Alles")
                                        {
                                                $_SESSION["SuchBenutzer"] = 'AND Benutzername = "'.$_POST['SuchBenutzer'].'"';
                                        }
                                        else
                                        {
                                                $_SESSION["SuchBenutzer"] ="";
                                        }
                                    // StatusAlg
                                        if ($_POST['status']!= "Alle")
                                        {
                                                $_SESSION["status"] = ' AND Stadium '.$_POST['status'].'';
                                        }
                                        else
                                        {
                                                $_SESSION["status"] ="";
                                        }

                                    // SortierAlg
                                        {
                                            if (isset($_POST['sortieren']))
                                            {
                                                    $_SESSION["sortierung"] = "ORDER BY ID ".$_POST['sortieren'];

                                            }
                                            else
                                            {
                                                $_SESSION["sortierung"] = "ORDER BY ID DESC";    
                                            }
                                        }

                                    $user= "SELECT * from arbeitspakete Where (Projekt_ID ='".$_SESSION["ProjektID"]."' AND Aktiv = 1 AND NOT Bezeichnung ='Urlaub' AND NOT Bezeichnung = 'Krankheit' ".$_SESSION["SuchBenutzer"]."".$_SESSION["status"].") ".$_SESSION["sortierung"]."";
                                }

                        ?>
                        <!-- Übersichttabelle erstellen -->
                        <br>
                            <?php
                            $test=array();
                            $ubersicht=array();
                            $anzahl = 0;
                            if($result = mysqli_query($db, $user))
                            {
                                $anzahl = $result->num_rows;
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    $ubersicht[$row['ID']]=$row;
                                    $_SESSION["APID"]=$row['ID'];
                                    $test[]=$row;
                                }
                            }
                            if(!isset($ubersicht))
                            {
                                echo "Diesem Projekt sind keine Arbeitspakete zugewiesen";
                                die;
                            }
                            
                                foreach($ubersicht as $value)
                                {
                                    echo '<form action="arbeitspakete.php" method="POST">';
                                    echo '<tr> 
                                            <td><input type="submit" class="myButton" name="apanpassen" value="Detailansicht"></td>
                                            <td><input type="hidden" name="ID" value="'.$value['ID'].'">'.$value['ID'].'</td>
                                            <td><input type="hidden" name="Bezeichnung" value="'.$value['Bezeichnung'].'">'.$value['Bezeichnung'].'</td>
                                            ';?>
                                            <?php if($value['Bezeichnung'] != "Urlaub" && $value['Bezeichnung'] != "Krankheit")
                                            {
                                                ?>
                                                <td><input type="hidden" name="geparbeitszeit" value="<?php $value['geplante_Arbeitszeit']?>"><?php echo number_format(floor($value['geplante_Arbeitszeit']) +((fmod($value['geplante_Arbeitszeit'],1)*0.6)),2,':','')?></td>
                                                <td><input type="hidden" name="istarbeitszeit" value="<?php $value['Ist_Arbeitszeit']?>"><?php echo number_format(floor($value['Ist_Arbeitszeit']) +((fmod($value['Ist_Arbeitszeit'],1)*0.6)),2,':','')?></td>
                                                <?php
                                                
                                            }
                                            else
                                            {
                                                ?>
                                                <td><input type="hidden" name="geparbeitszeit" value="<?php $value['geplante_Arbeitszeit']?>">0</td>
                                                <td><input type="hidden" name="istarbeitszeit" value="<?php $value['Ist_Arbeitszeit']?>">0</td>
                                                <?php
                                            }
                                            echo'
                                            <td><input type="hidden" name="Stadium" value="'.$value['Stadium'].'">'.$value['Stadium'].'</td>
                                            <td><input type="hidden" name="Benutzername" value="'.$value['Benutzername'].'">'.$value['Benutzername'].'</td>
                                            <td><input type="hidden" name="Arbeitspaketbeschreibung" value="'.$value['Arbeitspaketbeschreibung'].'">'.$value['Arbeitspaketbeschreibung'].'</td>
                                            <td><input type="hidden" name="kostenstelle" value="'.$value['Kostenstelle'].'">'.$value['Kostenstelle'].'</td>
                                            <td><input type="hidden" name="kostentraeger" value="'.$value['Kostentraeger'].'">'.$value['Kostentraeger'].'</td>
                                            <td><input type="hidden" name="kuerzeljira" value="'.$value['VerweisManagmentsystem'].'">'.$value['VerweisManagmentsystem'].'</td>'?>
                                            <?php
                                            if ( $value['Stadium'] == "100" )
                                            {
                                                ?>
                                                <td><input type="submit" class="abrechnung" name="abrechnen" value="Abschließen" onclick='return confirm("Sind Sie sich sicher das Sie das Arbeitspaket Abschließen wollen?")' formaction="projektdetailansicht.php"></td>
                                                <?php 
                                            }
                                            elseif ( $value['Ist_Arbeitszeit'] == "0" )
                                            {
                                                ?>
                                                <td><input type="submit" class="entfernen" name="Aploeschen" value="Löschen" formaction= "projektdetailansicht.php" onclick='return confirm("Sind Sie sich sicher das Sie das Arbeitspaket löschen wollen?")'></td>
                                                <?php 
                                            }
                                            else 
                                            {
                                                ?>
                                                <td></td>
                                                <?php 
                                            }
                                        echo'
                                        </tr>';
                                    echo '</form>';
                                }
                            ?>
                    </tbody>
                </table>
            </div>
    </body>
<html>