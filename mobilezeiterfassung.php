<!DOCTYPE html>
<html>
    <head>
        <title> Arbeitszeiterfassung </title>
        <div class="navigation">
            <div class="image">
                        <img src="images/ifano_logo_white.png" height="60" width="180">
            </div>
                    <?php
                        session_start();// SESSION initialisierung
                        // Logoutfunktion
                        if (isset($_POST["abmelden"]))
                        {
                            session_unset();
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }

                        // Kontrollabfrage, Zugriff nur durch ein Login-möglich
                        if (!isset($_SESSION["benutzername"]))
                        {
                            session_destroy();
                            header("Location: index.php");
                            die;
                        }
                        echo '<b>Benutzername: '.$_SESSION["benutzername"].'</b>';
                        $ID=$_SESSION["ID"];
                        //$_SESSION["benutzername"] = "Paetsch";
                        //$_SESSION["ID"] = 5;

                        //Verbindung zur Datenbank
                            $db = new mysqli("89.22.113.124:3306", "hippler", "Zufall11_Ifano7", "Morfi");
                            if ($db->connect_error)
                            {
                                die("Verbindung fehlgeschlagen: " . $db->connect_error);
                            }

                        // Stempel
                            $heute =getdate();
                            $Datumheute = $heute['year']."-".$heute['mon']."-".$heute['mday'];
                            $Zeit = date("h:i:sa", strtotime('now'));
                            if ( substr( $Zeit,8,2)=="pm")
                            {
                                $Stunde = substr( $Zeit,0,2)+12;
                                $Zeit = $Stunde.substr( $Zeit,2,6);
                            }
                            $TimeTable[]=array();

                            $Filter = "SELECT * FROM TimeTable Where ( UserID = '".$_SESSION["ID"]."' AND ToDate = '".$Datumheute."') ";

                            if($result = mysqli_query($db, $Filter))
                            {
                                while($row = mysqli_fetch_assoc($result))
                                {
                                    $TimeTable[]=$row;
                                }
                            }

                            if (!isset($TimeTable[1] ))
                            {
                                $a=9;
                            }
                            else
                            {
                                foreach ($TimeTable[1] as $index => $value)
                                {
                                    if(!isset($value) && isset($TimeTable[1]['IDTimeTable']))
                                    {
                                        $a = substr($index,0,1);
                                        break;
                                    }
                                    else
                                    {
                                        $a=7;
                                    }
                                }
                            }
                            $_SESSION["Stempel"]=$a;

                            if( $_SESSION["Stempel"] %2 == 0)
                            {
                                $buttonname= "Ausstempeln";
                            }
                            else
                            {
                                $buttonname="Einstempeln";
                            }
                    ?>
                    <table>
                        <tr>
                            <form action="<?php $_SERVER ['PHP_SELF'];?>" method="POST">

                                <!-- <td class = "menuborderless"><a href="Arbeitszeiterfassung.php" class="inputbutton">Zeiterfassung</a></td>-->

                                <td class = "menuborderless">

                                <?php


                                    if ( $_SESSION["Stempel"]!=7)
                                    {
                                       if($buttonname=="Einstempeln")
                                       {
                                         echo  '<a href="Stempelmobil.php" class="inputstempelneu" width = "50px">';
                                         echo $buttonname ;

                                       }

                                        else
                                        {
                                            ?>
                                            <a href="Stempelmobil.php" class="inputbuttonlogout2" width = "50px">
                                            <?php

                                            echo $buttonname;

                                        }
                                    }
                                ?>

                                </a></td>


                                <input type="submit" name="abmelden" value="Logout" class="inputbuttonlogout">

                    </form>
                  </table>

        </div>


        <div>
            <h2><u>Arbeitszeiterfassung IFANO</u></h2>
        </div>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <!--AJAX Life-Daten aus der Datenbank Funktion -->
            <script>
            function showfertigstellung(str)
            {
                if (str == "")
                {
                    document.getElementById("fertigstellen").innerHTML = "";
                    return;
                }
                else
                {
                    if (window.XMLHttpRequest)
                    {
                        xmlhttp = new XMLHttpRequest();
                    }
                    else
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function()
                    {
                        if (this.readyState == 4 && this.status == 200)
                        {
                            document.getElementById("fertigstellen").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET","fertigstellungajax.php?q="+str, true);
                    xmlhttp.send();
                }
            }
            function showpaket(str)
            {
                if (str == "")
                {
                    document.getElementById("paket").innerHTML = "";
                    return;
                }
                else
                {
                    if (window.XMLHttpRequest)
                    {
                        xmlhttp = new XMLHttpRequest();
                    }
                    else
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function()
                    {
                        if (this.readyState == 4 && this.status == 200)
                        {
                            document.getElementById("paket").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET","arbeitspaketajax.php?q="+str, true);
                    xmlhttp.send();
                }
            }
            function updateTextInput(val)
            {
                document.getElementById('textInput').value=val;
            }
            function updatedate(test)
            {
                var tage = document.getElementById('1').value=test;
                document.getElementById('2').value = tage;
            }
            </script>
        <?php



                if (isset($_POST['einfuegen']))
                {
                    //If-Abfrage zur Fehlervermeidung bei Checkboxen
                    if(!isset($_POST['sonstiges']))
                    {
                        $sonstigebesch = "";
                    }
                    else
                    {
                        $sonstigebesch = $_POST['beschreibung'];
                    }
                    if(!isset($_POST['code']))
                    {
                        $code = "";
                    }
                    else
                    {
                        $code = $_POST['code']."\n";
                    }
                    if(!isset($_POST['schulung']))
                    {
                        $schulung = "";
                    }
                    else
                    {
                        $schulung = $_POST['schulung']."\n";
                    }
                    if(!isset($_POST['dokumentation']))
                    {
                        $doku = "";
                    }
                    else
                    {
                        $doku = $_POST['dokumentation']."\n";
                    }
                    // Zeitstatus
                    if(!isset($_POST['Zeitstatus']))
                    {
                        $Zeitstatus = "0"; // nicht geplant
                    }
                    else
                    {
                        $Zeitstatus = "1"; // geplant
                    }



                    // Eingabe Nutzer
                    $datum= $_POST['datum'];
                    $datumbis = $_POST['datumbis'];
                    $zeita= $_POST['zeitbeginn'];
                    $projekt= $_POST['projekt'];
                    $arbeitspaket= $_POST['arbeitspaket'];
                    $beschreibung= $doku.$schulung.$code."Sonstiges: ".$sonstigebesch;
                    $fertigstellung= $_POST['fertigstellung'];
                    $Abwesendheit = $_POST['Abwesend'];

                    //Rechnung Zeit

                    $stunden = substr($zeita, 0, 2);
                    $stunden = $stunden + substr($zeita,3,2)/60;

                    // Anmeldung Datenbank mit "Einfügen-Funktion"
                    $sql = mysqli_query($db, "INSERT INTO arbeitszeit(Datum,Datumbis, Projekt, Arbeitspaket, Stunden, Benutzername, Beschreibung, Zeitstatus, Abwesenheit)
                    VALUES ('$datum','$datumbis','$projekt','$arbeitspaket','$stunden','".$_SESSION["benutzername"]."','$beschreibung','$Zeitstatus','$Abwesendheit')");

                    $update = mysqli_query($db, "UPDATE arbeitspakete SET Stadium = '$fertigstellung' WHERE ID = '$arbeitspaket'");

                }
                $abwesend= "SELECT * FROM Abwesenheitsstatus";
                $abwesenheitsstatus = array();

                if ($result = mysqli_query($db, $abwesend))
                {
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $abwesenheitsstatus[]=$row;
                    }

                }
            //Nimmt alle Datenbankeinträge die das Stadium Realiserung und Wo der Benutzer ein Arbeitspaket oder Projekt zugeordnet ist
                $user= "SELECT * from  Arbeiterzuordnung WHERE projektleiter ='".$_SESSION["benutzername"]."'";
                $allenutzer=array();
                $anzahl = 0;



                if($result = mysqli_query($db, $user))
                {
                    while($row = mysqli_fetch_object($result))
                    {
                        $allenutzer[]= "$row->ID $row->Projektname";
                    }
                }
                if($result = mysqli_query($db, $user))
                {
                    $anzahl = $result->num_rows;
                    while($row = mysqli_fetch_assoc($result))
                    {
                        $ubersicht[$row['ID']]=$row;
                    }
                }
        ?>
        <form action ="<?php $_SERVER ['PHP_SELF'];?>" method="POST" id="area">
            <div>
                <table> <border="1" cellspacing="2" cellpadding="3">
                    <!--Eingabe Nutzer-->
                    <tr><td class="tabelleohneinput">Datum:</td><td class="tabelleohneinput"><input type = "Date" style="width:507px" name = "datum" id="1" onchange="updatedate(this.value)"></td></tr>
                    <tr><td class="tabelleohneinput">Bis:(optional)</td><td class="tabelleohneinput"><input type = "Date" style="width:507px" name = "datumbis" id="2"></td></tr>
                    <tr><td class="tabelleohneinput">Zeit:</td><td class="tabelleohneinput"><input type = "time" step="900" min="00:15" max="10:00" style="width:507px" name = "zeitbeginn"></td></tr>
                    <form>
                        <tr><td class="tabelleohneinput">Projekt:</td><td class="tabelleohneinput"><select name="projekt" style="width:515px" id="dropdown"onchange="showpaket(this.value) ">
                            <option value ="0" selected="selected"> Bitte wählen </option>
                            <?php
                                print_r($allenutzer);
                                foreach($allenutzer as $value)
                                {
                                    echo '<option value="'.$value.'">'.$value.'</option>';
                                }
                            ?>
                        </select></td>
                    </form>
                    <tr><td class="tabelleohneinput">Arbeitspaket:</td><td class="tabelleohneinput"><div id="paket"></div></td></tr>
                    <tr><td class="tabelleohneinput">Fertigstellung:</td><td class="tabelleohneinput">
                    <div id="fertigstellen"></div></td></tr>
                    <tr><td class="tabelleohneinput">Details:</td><td class="tabelleohneinput">
                        <select name="Abwesend">
                                <?php
                                    foreach($abwesenheitsstatus as $value)
                                    {
                                        if($value['Abwesenheitsstatus'] == "IFANO")
                                        {
                                            echo '<option value="'.$value['Abwesenheitsstatus'].'" selected>'.$value['Abwesenheitsstatus'].'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$value['Abwesenheitsstatus'].'">'.$value['Abwesenheitsstatus'].'</option>';
                                        }
                                    }
                                ?>
                        <input type="Checkbox" name="Zeitstatus" value="0">Geplante Arbeitszeit
                    <tr><td class="tabelleohneinput">Beschreibung:</td><td class="tabelleohneinput">
                        <input type="Checkbox" name="dokumentation" value="Dokumentation">Dokumentation
                        <input type="Checkbox" name="schulung" value="Schulung">Schulung
                        <input type="Checkbox" name="code" value="Code-Anpassungen">Code-Anpassungen
                        <input type="Checkbox" name="sonstiges" value="Sonstiges">Sonstiges<br>
                        <Textarea form="area" cols="10" rows="10" class="textbereichzeit" name = "beschreibung"></textarea></td></tr>
                    <tr><td class="tabelle" colspan="2"><input type= "submit" class="myButton" name = "einfuegen" value = "Einfügen"></td></tr>
                </table>
                <br>
            </div>
        </form>
        <div class="wrapper">
        <table class="scroll" cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th>Auftrabgsnummer</th>
                    <th>Projektname</th>
                    <th>Kunde</th>
                    <th>Ist-Arbeitszeit (in h)</th>
                </tr>
            </thead>
                <tbody>
                    <?php
                        // erstell die Übersicht unter den Eingabefeldern
                        foreach($ubersicht as $value)
                        {
                            //$Zeit = floor($value['ist_arbeitszeit']) +( "0.".(fmod($value['ist_arbeitszeit'],1)*60));
                            echo '<form action="projektdetailansicht.php" method="POST">';
                            echo '<tr>
                                    <td><input type="hidden" name="Auftragsnummer" value="'.$value['Auftragsnummer'].'">'.$value['Auftragsnummer'].'</td>
                                    <td><input type="hidden" name="projektname" value="'.$value['Projektname'].'">'.$value['Projektname'].'</td>
                                    <td><input type="hidden" name="kunde" value="'.$value['Firmenname'].'">'.$value['Firmenname'].'</td>
                                    <td align="right"><input type="hidden" name="istarbeit" value="'.$value['ist_arbeitszeit'].'">'.number_format(floor($value['ist_arbeitszeit']) +((fmod($value['ist_arbeitszeit'],1)*0.6)),2,':','').'</td>

                                </tr>';
                            echo '</form>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
